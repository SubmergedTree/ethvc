package boilerplate

import (
	"crypto/sha256"
	"encoding/base64"
)

func CalcHash(toHash string) string {
	hasher := sha256.New()
	hasher.Write([]byte(toHash))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	return sha
}

/*
func ToJson(indexFile *domain_legacy.IndexFile) (string, error) {
	bytes, err := json.Marshal(indexFile)
	return string(bytes), err
}
*/
