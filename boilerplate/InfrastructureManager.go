package boilerplate

type InfrastructureManager interface {
	GetUI() UserInterface
	GetPersist() Persist
	GetFileIO() FileIO
	GetRemoteMetaConnector() RemoteMetaConnector
	GetRemoteDataConnector() RemoteDataConnector
}

type InfrastructureManagerImpl struct {
	UI      UserInterface
	Persist Persist
	FileIO  FileIO
	RMC     RemoteMetaConnector
	RDC     RemoteDataConnector
}

func NewInfrastructureManagerImpl(ui UserInterface,
	persist Persist,
	fileIO FileIO,
	rmc RemoteMetaConnector,
	rdc RemoteDataConnector) InfrastructureManager {
	return &InfrastructureManagerImpl{ui, persist, fileIO, rmc, rdc}
}

func (f *InfrastructureManagerImpl) GetUI() UserInterface {
	return f.UI
}

func (f *InfrastructureManagerImpl) GetPersist() Persist {
	return f.Persist
}

func (f *InfrastructureManagerImpl) GetFileIO() FileIO {
	return f.FileIO
}

func (f *InfrastructureManagerImpl) GetRemoteMetaConnector() RemoteMetaConnector {
	return f.RMC
}

func (f *InfrastructureManagerImpl) GetRemoteDataConnector() RemoteDataConnector {
	return f.RDC
}
