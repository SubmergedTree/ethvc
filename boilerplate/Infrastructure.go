package boilerplate

// TODO return if operation was successful
type RemoteMeta interface {
	AddUser(newUserPublicKey string, newUserEmail string, newUserRightLevel int64) error
	DeleteUser(userPublicKey string) error
	UpdateUser(newUserEmail string, userPublicKey string, rightLevel int64) error
	GetUsers() ([]*UserMetaPOD, error)

	GetBranches() ([]*BranchMetaPOD, error)
	CreateBranch(branchName string, unixTimeStamp int64, accessRight int64) error
	AddCommit(remoteFileAddress string, branchName string, revision int64) error
}

type RemoteData interface {
	UploadFile(path string) (string, error)
	GetFile(hash string) (string, error)
	GetFileAsBytes(hash string) ([]byte, error)

	UploadString(content string) (string, error)
}

type UserInterface interface {
	Show(message string)
	ShowGeneric(a ...interface{})
	Run() UsecaseTO
	Ask(question string) string
}

type Persist interface {
	Serialize(obj interface{}) error
	Deserialize() (interface{}, error)
}

type FileIO interface {
	GetAllFilePaths(top string) ([]string, error)
	ReadFile(path string) (string, error)

	NestedWrite(path string, content string) error
	DeleteAll() error
}

type RemoteMetaConnector interface {
	Create(url string, privateKey string, email string) (RemoteMeta, string, error)
	Connect(url string, contractAddress string, privateKey string) (RemoteMeta, error)
}

type RemoteDataConnector interface {
	Connect(url string) RemoteData
}

type UsecaseTO interface {
	GetUsecaseName() string
}

type BranchMetaPOD struct {
	Name              string
	AccessRights      uint64
	CreationDateUnix  uint64
	LastCommitAddress string
	Revision          int64
}

type UserMetaPOD struct {
	Email      string
	RightLevel uint64
	Address    string
}
