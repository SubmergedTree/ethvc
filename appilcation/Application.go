package appilcation

type Application interface {
	/*CreateLocalRepository(email string) string
	CreateRemote(url, privateKey string)string
	GetRemoteBranches(url, address, privateKey string)string

	CreateBranch(branchName string)string
	ChangeBranch(branchName string)string
	Commit(commitName string)string
	Merge(branchToMerge string)string

	GetAllUser(url, address, privateKey string)string
	CreateUser(url, address, privateKey, email, publicKey string, rightLevel int64)string
	UpdateUser(url, address, privateKey, email, publicKey string, rightLevel int64)string
	DeleteUser(url, address, privateKey, publicKeyToDelete string)string

	Push(url, address, privateKey string)string
	Pull(url, address, privateKey, branchName string)string
	*/
	Run()
}

type CreateLocalRepository struct {
	UsecaseName string

	Email string
}

func (u *CreateLocalRepository) GetUsecaseName() string {
	return u.UsecaseName
}

type CreateRemote struct {
	UsecaseName string

	Url        string
	PrivateKey string
	Email      string
}

func (u *CreateRemote) GetUsecaseName() string {
	return u.UsecaseName
}

type GetRemoteBranches struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
}

func (u *GetRemoteBranches) GetUsecaseName() string {
	return u.UsecaseName
}

type CreateBranch struct {
	UsecaseName string

	BranchName  string
	AccessRight uint
}

func (u *CreateBranch) GetUsecaseName() string {
	return u.UsecaseName
}

type ChangeBranch struct {
	UsecaseName string

	BranchName string
}

func (u *ChangeBranch) GetUsecaseName() string {
	return u.UsecaseName
}

type Commit struct {
	UsecaseName string

	CommitName string
}

func (u *Commit) GetUsecaseName() string {
	return u.UsecaseName
}

type Merge struct {
	UsecaseName string

	BranchToMerge string
}

func (u *Merge) GetUsecaseName() string {
	return u.UsecaseName
}

type GetAllUser struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
}

func (u *GetAllUser) GetUsecaseName() string {
	return u.UsecaseName
}

type CreateUser struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
	Email      string
	PublicKey  string
	RightLevel int64
}

func (u *CreateUser) GetUsecaseName() string {
	return u.UsecaseName
}

type UpdateUser struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
	Email      string
	PublicKey  string
	RightLevel int64
}

func (u *UpdateUser) GetUsecaseName() string {
	return u.UsecaseName
}

type DeleteUser struct {
	UsecaseName string

	Url               string
	Address           string
	PrivateKey        string
	PublicKeyToDelete string
}

func (u *DeleteUser) GetUsecaseName() string {
	return u.UsecaseName
}

type Push struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
}

func (u *Push) GetUsecaseName() string {
	return u.UsecaseName
}

type Pull struct {
	UsecaseName string

	Url        string
	Address    string
	PrivateKey string
	BranchName string
}

func (u *Pull) GetUsecaseName() string {
	return u.UsecaseName
}
