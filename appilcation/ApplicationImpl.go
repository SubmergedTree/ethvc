package appilcation

import (
	"ethVC/boilerplate"
	"ethVC/domain"
	"fmt"
)

type ApplicationImpl struct {
	InfrastructureManager boilerplate.InfrastructureManager
}

func (a *ApplicationImpl) createLocalRepository(to boilerplate.UsecaseTO) {
	email := to.(*CreateLocalRepository).Email
	repoFactory := a.localSetUp()
	repo := repoFactory.NewRepository(email)
	err := repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) createRemote(to boilerplate.UsecaseTO) {
	url := to.(*CreateRemote).Url
	privateKey := to.(*CreateRemote).PrivateKey
	email := to.(*CreateRemote).Email

	_, address, err := a.createRemoteSetup(url, email, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	a.InfrastructureManager.GetUI().Show(fmt.Sprintf("Contract address: %s", address))

	/*repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		fmt.Println(err.Error())
		return
	}*/
}

func (a *ApplicationImpl) getRemoteBranches(to boilerplate.UsecaseTO) {
	url := to.(*GetRemoteBranches).Url
	address := to.(*GetRemoteBranches).Address
	privateKey := to.(*GetRemoteBranches).PrivateKey

	repoFactory, err := a.connectToRemoteSetup(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	branchNames, err := repo.GetRemoteBranchNames()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	a.InfrastructureManager.GetUI().Show("Available branches: \n")
	for _, branchName := range branchNames {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("%s \n", branchName))
	}

}

func (a *ApplicationImpl) createBranch(to boilerplate.UsecaseTO) {
	branchName := to.(*CreateBranch).BranchName
	accessRight := to.(*CreateBranch).AccessRight
	repoFactory := a.localSetUp()
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		fmt.Println(err.Error())
	}
	success, err := repo.AddBranch(branchName, accessRight)
	if err != nil {
		fmt.Println(err.Error())
	}
	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
	a.InfrastructureManager.GetUI().Show(fmt.Sprintf("Success: %b", success))
}

func (a *ApplicationImpl) changeBranch(to boilerplate.UsecaseTO) {
	branchName := to.(*ChangeBranch).BranchName
	repoFactory := a.localSetUp()
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		fmt.Println(err.Error())
	}
	repo.ChangeBranch(branchName)

	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) commit(to boilerplate.UsecaseTO) {
	commitName := to.(*Commit).CommitName
	repoFactory := a.localSetUp()
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	success, err := repo.Commit(commitName)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	if !success {
		fmt.Println("Not socczss")
	}
	//fmt.Println(repo)
	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) merge(to boilerplate.UsecaseTO) {
	branchToMerge := to.(*Merge).BranchToMerge
	repoFactory := a.localSetUp()
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	success, err := repo.MergeBranchIntoCurrentBranch(branchToMerge)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	if !success {
		fmt.Println("not successfully")
		return
	}

	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) getAllUser(to boilerplate.UsecaseTO) {
	url := to.(*GetAllUser).Url
	address := to.(*GetAllUser).Address
	privateKey := to.(*GetAllUser).PrivateKey

	remoteMeta, err := a.InfrastructureManager.GetRemoteMetaConnector().Connect(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	userMetaPods, err := remoteMeta.GetUsers()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	a.InfrastructureManager.GetUI().Show("Found users: \n")
	for _, userMeta := range userMetaPods {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("%s %s %d", userMeta.Email, userMeta.Address, userMeta.RightLevel))
	}

}

func (a *ApplicationImpl) createUser(to boilerplate.UsecaseTO) {
	url := to.(*CreateUser).Url
	address := to.(*CreateUser).Address
	privateKey := to.(*CreateUser).PrivateKey
	email := to.(*CreateUser).Email
	publicKey := to.(*CreateUser).PublicKey
	rightLevel := to.(*CreateUser).RightLevel

	remoteMeta, err := a.InfrastructureManager.GetRemoteMetaConnector().Connect(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	err = remoteMeta.AddUser(publicKey, email, rightLevel)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
	}
}

func (a *ApplicationImpl) updateUser(to boilerplate.UsecaseTO) {
	url := to.(*UpdateUser).Url
	address := to.(*UpdateUser).Address
	privateKey := to.(*UpdateUser).PrivateKey
	email := to.(*UpdateUser).Email
	publicKey := to.(*UpdateUser).PublicKey
	rightLevel := to.(*UpdateUser).RightLevel

	remoteMeta, err := a.InfrastructureManager.GetRemoteMetaConnector().Connect(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	err = remoteMeta.UpdateUser(email, publicKey, rightLevel)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
}

func (a *ApplicationImpl) deleteUser(to boilerplate.UsecaseTO) {
	url := to.(*DeleteUser).Url
	address := to.(*DeleteUser).Address
	privateKey := to.(*DeleteUser).PrivateKey
	publicKeyToDelete := to.(*DeleteUser).PublicKeyToDelete

	remoteMeta, err := a.InfrastructureManager.GetRemoteMetaConnector().Connect(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	err = remoteMeta.DeleteUser(publicKeyToDelete)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

}

func (a *ApplicationImpl) push(to boilerplate.UsecaseTO) {
	url := to.(*Push).Url
	address := to.(*Push).Address
	privateKey := to.(*Push).PrivateKey

	repoFactory, err := a.connectToRemoteSetup(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	success, err := repo.Push()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	if !success {
		a.InfrastructureManager.GetUI().Show("Your local branch is not up to date, please pull first. \n")
		return
	}

	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) pull(to boilerplate.UsecaseTO) {
	url := to.(*Pull).Url
	address := to.(*Pull).Address
	privateKey := to.(*Pull).PrivateKey
	//branchName := to.(*Pull).BranchName

	repoFactory, err := a.connectToRemoteSetup(url, address, privateKey)
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	repo, err := repoFactory.NewRepositoryFromFile()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}

	success, err := repo.Pull()
	if err != nil {
		a.InfrastructureManager.GetUI().Show(fmt.Sprintf("An error occurred: %s", err.Error()))
		return
	}
	if !success {
		a.InfrastructureManager.GetUI().Show("Your local branch is not up to date, please pull first. \n")
		return
	}

	err = repo.SaveToFile()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (a *ApplicationImpl) Run() {
	//	a.push(&Push{"Push", "http://127.0.0.1:8543",
	//		"0x01C6eBf5808Bb095BB9ee57DCbA3D9d89350815a", "e62e873eb242a90d48f127f7fcd74c75565d75573b27bf6bab06c5bc6f5dc673"})

	//	a.pull(&Pull{"Pull", "http://127.0.0.1:8543",
	//	"0x01C6eBf5808Bb095BB9ee57DCbA3D9d89350815a", "e62e873eb242a90d48f127f7fcd74c75565d75573b27bf6bab06c5bc6f5dc673", "light"})
	usecaseTO := a.InfrastructureManager.GetUI().Run()
	switch usecaseTO.GetUsecaseName() {
	case "CreateLocalRepository":
		a.createLocalRepository(usecaseTO)
		break
	case "CreateBranch":
		a.createBranch(usecaseTO)
		break
	case "Commit":
		a.commit(usecaseTO)
		break
	case "ChangeBranch":
		a.changeBranch(usecaseTO)
		break
	case "Merge":
		a.merge(usecaseTO)
		break
	case "CreateRemote":
		a.createRemote(usecaseTO)
		break
	case "GetRemoteBranches":
		a.getRemoteBranches(usecaseTO)
		break
	case "CreateUser":
		a.createUser(usecaseTO)
		break
	case "GetAllUser":
		a.getAllUser(usecaseTO)
		break
	case "UpdateUser":
		a.updateUser(usecaseTO)
		break
	case "DeleteUser":
		a.deleteUser(usecaseTO)
		break
	case "Push":
		a.push(usecaseTO)
		break
	case "Pull":
		a.pull(usecaseTO)
		break
	}
}

func (a *ApplicationImpl) localSetUp() domain.RepositoryFactory {
	fileIO := a.InfrastructureManager.GetFileIO()
	persist := a.InfrastructureManager.GetPersist()
	ui := a.InfrastructureManager.GetUI()
	fileFactory := &domain.FileFactoryImpl{nil, ui, fileIO}
	commitFactory := &domain.CommitFactoryImpl{nil, fileFactory, fileIO}
	branchFactory := &domain.BranchImplFactory{nil, commitFactory}
	repositoryFactory := &domain.RepositoryFactoryImpl{nil, persist, branchFactory}
	return repositoryFactory
}

func (a *ApplicationImpl) connectToRemoteSetup(metaUrl, conntractAddress, privateKey string) (domain.RepositoryFactory, error) {
	fileIO := a.InfrastructureManager.GetFileIO()
	persist := a.InfrastructureManager.GetPersist()
	ui := a.InfrastructureManager.GetUI()
	remoteData := a.InfrastructureManager.GetRemoteDataConnector().Connect("")
	remoteMeta, err := a.InfrastructureManager.GetRemoteMetaConnector().Connect(metaUrl, conntractAddress, privateKey)
	if err != nil {
		return nil, err
	}

	fileFactory := &domain.FileFactoryImpl{remoteData, ui, fileIO}
	commitFactory := &domain.CommitFactoryImpl{remoteData, fileFactory, fileIO}
	branchFactory := &domain.BranchImplFactory{remoteMeta, commitFactory}
	repositoryFactory := &domain.RepositoryFactoryImpl{remoteMeta, persist, branchFactory}
	return repositoryFactory, nil
}

func (a *ApplicationImpl) createRemoteSetup(metaUrl, email, privateKey string) (domain.RepositoryFactory, string, error) {
	fileIO := a.InfrastructureManager.GetFileIO()
	persist := a.InfrastructureManager.GetPersist()
	ui := a.InfrastructureManager.GetUI()
	remoteData := a.InfrastructureManager.GetRemoteDataConnector().Connect("")
	remoteMeta, address, err := a.InfrastructureManager.GetRemoteMetaConnector().Create(metaUrl, privateKey, email)
	if err != nil {
		return nil, "", err
	}

	fileFactory := &domain.FileFactoryImpl{remoteData, ui, fileIO}
	commitFactory := &domain.CommitFactoryImpl{remoteData, fileFactory, fileIO}
	branchFactory := &domain.BranchImplFactory{remoteMeta, commitFactory}
	repositoryFactory := &domain.RepositoryFactoryImpl{remoteMeta, persist, branchFactory}
	return repositoryFactory, address, nil
}
