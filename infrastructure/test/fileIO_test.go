package test

import (
	"ethVC/infrastructure"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func TestGetAllFilePaths(t *testing.T) {
	should := []string{"data/bar/bazz/honey.go", "data/bar/bazz/jam.go", "data/bar/bread.go", "data/foo/peanutbutter.go"}
	fileIO := infrastructure.NewFileIO()
	is, err := fileIO.GetAllFilePaths("./data")

	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(is, should) {
		fmt.Println(is)
		fmt.Println("does not equals")
		fmt.Println(should)
		t.Fail()
	}
}

func TestReadFile(t *testing.T) {
	fileIO := infrastructure.NewFileIO()
	content, err := fileIO.ReadFile("./data/foo/peanutbutter.go")

	if err != nil {
		t.Error(err)
	}

	should := "package foo\n" +
		"\n" +
		"import \"fmt\"\n" +
		"\n" +
		"func peanutButter() {\n" +
		"	fmt.Println(\"foobar\")\n" +
		"}\n"

	if !(content == should) {
		t.Error("content is wrong")
	}
}

func TestNestedWrite(t *testing.T) {
	fileIO := infrastructure.NewFileIO()
	err := fileIO.NestedWrite("./blub/blab/foo.txt", "blab")
	if err != nil {
		t.Error(err)
	}

	read, _ := ioutil.ReadFile("./blub/blab/foo.txt")

	if string(read) == "blab" {
		os.RemoveAll("./blub/blab/foo.txt")
		os.RemoveAll("./blub/blab")
		os.RemoveAll("./blub")
	} else {
		t.Fail()
	}
}
