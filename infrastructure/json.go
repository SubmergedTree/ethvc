package infrastructure

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Json struct {
	path string
}

func NewJson() (s *Json) {
	return &Json{"./ethVC_local.json"}
}

func (s *Json) Serialize(obj interface{}) error {
	file, err := os.Create(s.path)
	if err == nil {
		m, err := json.Marshal(obj)
		if err != nil {
			return err
		}
		fmt.Println(m)
		file.Write(m)
	}
	file.Close()
	return err
}

func (s *Json) Deserialize() (interface{}, error) {
	var obj interface{}
	b, err := ioutil.ReadFile(s.path)
	if err == nil {
		if err := json.Unmarshal(b, &obj); err != nil {
			return nil, err
		}
	}
	return obj, err
}
