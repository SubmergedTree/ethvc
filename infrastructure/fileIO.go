package infrastructure

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type FileIO struct{}

func NewFileIO() *FileIO {
	return &FileIO{}
}

func (f *FileIO) GetAllFilePaths(top string) ([]string, error) {
	var result []string

	err := filepath.Walk(top, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		fileInfo, err := os.Stat(path)
		if err != nil {
			return err
		}
		if fileInfo.Name() == "ethVC_local.json" {
			return nil
		}
		if fileInfo.Mode().IsRegular() {
			result = append(result, path)
		}
		return nil
	})

	if err != nil {
		log.Println(err)
	}
	return result, nil
}

func (f *FileIO) ReadFile(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func (f *FileIO) NestedWrite(filePath string, content string) error {
	var err error
	path, _, inDir := splitPathAndFileName(filePath)
	if inDir {
		err = os.MkdirAll(path, os.ModePerm)

	}
	err = ioutil.WriteFile(filePath, []byte(content), 0644)
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}
	return nil
}

func (f *FileIO) DeleteAll() error {
	rescued, err := ioutil.ReadFile("ethVC_local.json")
	if err != nil {
		return err
	}
	err = os.RemoveAll(".")
	if err != nil {
		//return err
	}

	err = ioutil.WriteFile("ethVC_local.json", rescued, 0644)
	if err != nil {
		return err
	}
	return nil
}

func splitPathAndFileName(filePath string) (path string, file string, inDirectory bool) {
	lastSlashIndex := strings.LastIndex(filePath, "/")
	if lastSlashIndex == -1 {
		return "", "", false
	} else {
		return filePath[0:lastSlashIndex], filePath[lastSlashIndex:], true
	}
}
