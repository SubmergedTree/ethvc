package infrastructure

import (
	"context"
	"crypto/ecdsa"
	"ethVC/ContractImpl"
	"ethVC/boilerplate"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
	"strings"
)

type RemoteMetaConnectorImpl struct {
}

func (RemoteMetaConnectorImpl) Create(url string, privateKey string, email string) (boilerplate.RemoteMeta, string, error) {
	return NewEthContract(url, privateKey, email)
}

func (RemoteMetaConnectorImpl) Connect(url string, contractAddress string, privateKey string) (boilerplate.RemoteMeta, error) {
	return ConnectToEthContract(url, contractAddress, privateKey)
}

type EthContract struct {
	client          *ethclient.Client
	auth            *bind.TransactOpts
	contractAddress common.Address
	contract        *ContractImpl.EthVC
	ethclientUrl    string
}

func NewEthContract(ethclientUrl string, privateKey string, email string) (boilerplate.RemoteMeta, string, error) {
	client, err := ethclient.Dial(ethclientUrl)

	if err != nil {
		return nil, "", err
	}

	privateKeyECDSA, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return nil, "", err
	}

	publicKey := privateKeyECDSA.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, "", err
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		return nil, "", err
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, "", err
	}

	_ = gasPrice

	auth := bind.NewKeyedTransactor(privateKeyECDSA)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	//auth.GasLimit = uint64(20000000)
	//auth.GasPrice = gasPrice
	auth.GasLimit = uint64(0) //uint64(20000000)
	auth.GasPrice = nil       //big.NewInt(7845131)//gasPrice

	emailBytes := [32]byte{}
	copy(emailBytes[:], []byte(email))

	address, tx, instance, err := ContractImpl.DeployEthVC(auth, client, emailBytes)
	if err != nil {
		return nil, "", err
	}

	_ = tx

	return &EthContract{client, auth, address, instance, ethclientUrl}, address.String(), nil
}

func ConnectToEthContract(ethclientUrl string, contractAddress string, privateKey string) (boilerplate.RemoteMeta, error) {
	client, err := ethclient.Dial(ethclientUrl)
	if err != nil {
		return nil, err
	}
	address := common.HexToAddress(contractAddress)
	instance, err := ContractImpl.NewEthVC(address, client)
	if err != nil {
		return nil, err
	}

	privateKeyECDSA, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return nil, err
	}

	publicKey := privateKeyECDSA.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, err
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		return nil, err
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		return nil, err
	}

	_ = gasPrice

	auth := bind.NewKeyedTransactor(privateKeyECDSA)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(0) //uint64(20000000)
	auth.GasPrice = nil       //big.NewInt(7845131)//gasPrice

	return &EthContract{client, auth, address, instance, ethclientUrl}, nil
}

func (e *EthContract) AddUser(newUserPublicKey string, newUserEmail string, newUserRightLevel int64) error {
	publicKeyECDSA := common.HexToAddress(newUserPublicKey)
	email := [32]byte{}
	copy(email[:], []byte(newUserEmail))

	_, err := e.contract.AddAuthorizedUser(e.auth, publicKeyECDSA, email, big.NewInt(newUserRightLevel))
	if err != nil {
		return err
	}

	success, err := e.listenToEvents([]uint64{USER_CREATED_SUCCESS}, []uint64{USER_CREATED_FAILED}, userEval)
	fmt.Println(success)
	return nil
}

func (e *EthContract) DeleteUser(userPublicKey string) error {
	address := common.HexToAddress(userPublicKey)
	_, err := e.contract.DeleteUser(e.auth, address)
	if err != nil {
		return err
	}

	success, err := e.listenToEvents([]uint64{USER_DELETED_SUCCESS}, []uint64{USER_DELETED_FAILED}, userEval)
	fmt.Println(success)

	return nil
}

func (e *EthContract) UpdateUser(newUserEmail string, userPublicKey string, rightLevel int64) error {
	publicKeyECDSA := common.HexToAddress(userPublicKey)
	email := [32]byte{}
	copy(email[:], []byte(newUserEmail))

	_, err := e.contract.UpdateUser(e.auth, publicKeyECDSA, email, big.NewInt(rightLevel))
	if err != nil {
		return err
	}

	success, err := e.listenToEvents([]uint64{USER_UPDATED_SUCCESS}, []uint64{USER_UPDATED_FAILED}, userEval)
	fmt.Println(success)

	return nil
}

func (e *EthContract) GetUsers() ([]*boilerplate.UserMetaPOD, error) {
	count, err := e.contract.GetUserCount(nil)
	if err != nil {
		return nil, err
	}

	var users []*boilerplate.UserMetaPOD

	for i := uint64(0); i < count.Uint64(); i++ {
		userAddress, _ := e.contract.GetUserAtIndex(nil, big.NewInt(int64(i)))
		email, _ := e.contract.GetUserEmailAtIndex(nil, big.NewInt(int64(i)))
		rightLevel, _ := e.contract.GetUserRightLevelAtIndex(nil, big.NewInt(int64(i)))

		users = append(users, &boilerplate.UserMetaPOD{Address: userAddress.String(),
			Email:      string(email[:clen32(email)]),
			RightLevel: rightLevel.Uint64()})
	}
	return users, nil
}

func (e *EthContract) GetBranches() ([]*boilerplate.BranchMetaPOD, error) {
	var branches []*boilerplate.BranchMetaPOD
	names, err := e.contract.GetBranchNames(nil)
	if err != nil {
		return nil, err
	}

	for _, name := range names {
		name, creationDate, accessRight, lastCommitAddressUpper, lastCommitAddressLower, revision, err := e.contract.GetBranchByName(nil, name)
		if err != nil {
			return nil, err
		}

		var upperStr, lowerStr string
		upperStr = string(lastCommitAddressUpper[:clen32(lastCommitAddressUpper)])
		lowerStr = string(lastCommitAddressLower[:clen32(lastCommitAddressLower)])

		branches = append(branches, &boilerplate.BranchMetaPOD{Name: string(name[:clen32(name)]), AccessRights: accessRight.Uint64(),
			CreationDateUnix: creationDate.Uint64(), LastCommitAddress: upperStr + lowerStr, Revision: revision.Int64()})
	}

	return branches, nil
}

func (e *EthContract) CreateBranch(branchName string, unixTimeStamp int64, accessRight int64) error {
	branchNameBytes := [32]byte{}
	copy(branchNameBytes[:], []byte(branchName))

	_, err := e.contract.AddBranch(e.auth, big.NewInt(accessRight), branchNameBytes, big.NewInt(unixTimeStamp))
	if err != nil {
		fmt.Println("Error on create branch")
		return err
	}
	success, err := e.listenToEvents([]uint64{BRANCH_CREATED_SUCCESS}, []uint64{BRANCH_CREATED_ERROR_ALREADY_EXISTS,
		BRANCH_CREATED_ERROR_RIGHT_LEVEL_TO_LOW}, branchEval)
	fmt.Println(success)

	return nil
}

func (e *EthContract) AddCommit(remoteFileAddress string, branchName string, branchRevision int64) error { // commits (only one branch per time) // TODO first execution panics
	branchNameBytes := [32]byte{}
	copy(branchNameBytes[:], []byte(branchName))
	upper, lower := divide64StringToUpperLowerBytes(remoteFileAddress)
	//upperStr := string(upper[:clen32(upper)])
	//lowerStr := string(lower[:clen32(lower)])

	_, err := e.contract.AddCommit(e.auth, branchNameBytes, upper, lower, big.NewInt(branchRevision))
	if err != nil {
		return err
	}
	success, err := e.listenToEvents([]uint64{ADD_INDEX_FILE_SUCCESS}, []uint64{
		ADD_INDEX_FILE_FAILED,
		ADD_INDEX_FILE_BRANCH_DOES_NOT_EXIST,
		ADD_INDEX_FILE_RIGHT_LEVEL_TO_LOW,
		ADD_INDEX_FILE_TO_LOW_REVISION}, branchEval)
	fmt.Println(success)

	return nil
}

func branchEval(vLog types.Log, acceptEvents, failEvents []uint64, abi abi.ABI) (bool, error) {
	event := struct {
		BranchName  [32]byte
		SuccessCode *big.Int
	}{}

	err := abi.Unpack(&event, "VCEvent", vLog.Data)
	if err != nil {
		return false, err
	}
	//fmt.Println(string(event.BranchName[:clen32(event.BranchName)]))
	//fmt.Println(event.SuccessCode.Uint64())

	if uint64SliceContains(event.SuccessCode.Uint64(), acceptEvents) {
		//domain_legacy.UserInterface.Show("Operation was successful")
		return true, nil
	} else {
		return false, nil

	}
}

func userEval(vLog types.Log, acceptEvents, failEvents []uint64, abi abi.ABI) (bool, error) {
	event := struct {
		Address     common.Address
		SuccessCode *big.Int
	}{}

	err := abi.Unpack(&event, "VCEvent", vLog.Data)
	if err != nil {
		return false, err
	}
	//fmt.Println(event.Address.String())
	//fmt.Println(event.SuccessCode.Uint64())

	if uint64SliceContains(event.SuccessCode.Uint64(), acceptEvents) {
		//domain_legacy.UserInterface.Show("Operation was successful")
		return true, nil
	} else {
		return false, nil
	}
}

const (
	USER_CREATED_FAILED  = 0
	USER_CREATED_SUCCESS = 1
	USER_UPDATED_FAILED  = 2
	USER_UPDATED_SUCCESS = 3
	USER_DELETED_FAILED  = 4
	USER_DELETED_SUCCESS = 5

	REPOSITORY_CREATED = 6

	BRANCH_CREATED_SUCCESS                  = 7
	BRANCH_CREATED_ERROR_ALREADY_EXISTS     = 8
	BRANCH_CREATED_ERROR_RIGHT_LEVEL_TO_LOW = 9

	ADD_INDEX_FILE_SUCCESS               = 10
	ADD_INDEX_FILE_FAILED                = 11
	ADD_INDEX_FILE_BRANCH_DOES_NOT_EXIST = 12
	ADD_INDEX_FILE_RIGHT_LEVEL_TO_LOW    = 13
	ADD_INDEX_FILE_TO_LOW_REVISION       = 14
)

type evalListener func(vLog types.Log, acceptEvents, failEvents []uint64, abi abi.ABI) (bool, error)

func (e *EthContract) listenToEvents(acceptEvents, failEvents []uint64, eval evalListener) (bool, error) {
	ethclientUrl := e.ethclientUrl
	contractAddress := e.contractAddress.String()

	//ethclientUrl = "ws://127.0.0.1:8546" // TODO
	ethclientUrl = "ws://127.0.0.1:8543" // TODO
	client, err := ethclient.Dial(ethclientUrl)
	if err != nil {
		return false, err
	}

	contractAddressEth := common.HexToAddress(contractAddress)
	query := ethereum.FilterQuery{
		Addresses: []common.Address{contractAddressEth},
	}
	logs := make(chan types.Log)
	sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
	if err != nil {
		log.Fatal(err)
	}

	contractAbi, err := abi.JSON(strings.NewReader(string(ContractImpl.EthVCABI)))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("start listening")
	for {
		select {
		case err := <-sub.Err():
			log.Fatal(err)
			return false, err
		case vLog := <-logs:
			return eval(vLog, acceptEvents, failEvents, contractAbi)
		}
	}
	fmt.Println("should not appear")
	return false, nil
}

func uint64SliceContains(toCheck uint64, slice []uint64) bool {
	for _, num := range slice {
		if num == toCheck {
			return true
		}
	}
	return false
}

func clen32(bytes [32]byte) int {
	for i := 0; i < len(bytes); i++ {
		if bytes[i] == 0 {
			return i
		}
	}
	return len(bytes)
}

func divide64StringToUpperLowerBytes(str64 string) ([32]byte, [32]byte) {
	var upper [32]byte
	var lower [32]byte
	upperByteIdx := 0
	lowerByteIdx := 0
	counter := 0
	for idx := range str64 {
		if counter < 32 {
			upper[upperByteIdx] = str64[idx]
			upperByteIdx++
		} else {
			lower[lowerByteIdx] = str64[idx]
			lowerByteIdx++
		}
		counter++
	}
	return upper, lower
}
