package infrastructure

import (
	"bufio"
	"ethVC/appilcation"
	"ethVC/boilerplate"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type CLI struct {
}

func (c *CLI) Show(message string) {
	fmt.Println(message)
}

func (c *CLI) ShowGeneric(a ...interface{}) {
	fmt.Println(a)
}

func (c *CLI) Run() boilerplate.UsecaseTO {
	command := flag.String("command", "", "choose command To Execute:\n"+
		" - init: init local Repo\n"+
		"- config: set email and public key")
	flag.Parse()
	fmt.Println(*command)
	return c.executeCommand(*command)
}

func (c *CLI) Ask(question string) string {
	return ask(question)
}

func ask(question string) string {
	fmt.Println(question)
	reader := bufio.NewReader(os.Stdin)
	answer, _ := reader.ReadString('\n')
	answer = strings.TrimSuffix(answer, "\n")
	answer = strings.TrimSuffix(answer, " ")
	return answer
}

func (c *CLI) executeCommand(command string) boilerplate.UsecaseTO {
	reader := bufio.NewReader(os.Stdin)
	switch command {
	case "init":
		return c.createLocalRepo(reader)
		break
	//case "config":
	//	c.config(reader)
	//	break
	case "createBranch":
		return c.createBranch(reader)
		break
	case "changeBranch":
		return c.changeBranch(reader)
		break
	case "commit":
		return c.commit(reader)
		break
	case "push":
		return c.push(reader)
		break
	case "pull":
		return c.pull(reader)
		break
	case "getRemoteBranches":
		return c.getRemoteBranches()
		break
	case "createRemote":
		return c.createRemote(reader)
		break
	case "getUsers":
		return c.getUsers(reader)
		break
	case "addUser":
		return c.addUser(reader)
		break
	case "updateUser":
		return c.updateUser(reader)
		break
	case "deleteUser":
		return c.deleteUser(reader)
		break
	case "merge":
		return c.merge()
		break ////
	default:
		return nil
		break
	}
	return nil
}

func (c *CLI) createLocalRepo(reader *bufio.Reader) boilerplate.UsecaseTO {
	email := ask("Enter your email address")
	//publicKey := ask("Enter your public key")
	//res := (c.Application.CreateLocalRepository(email))
	//fmt.Println(res)
	//return email
	return &appilcation.CreateLocalRepository{"CreateLocalRepository", email}
}

func (c *CLI) createBranch(reader *bufio.Reader) boilerplate.UsecaseTO {
	branchName := ask("Enter branch name")
	rightLevel := ask("Enter right level")
	//fmt.Println(c.Application.CreateBranch(branchName))
	rightLevelInt, err := strconv.ParseInt(rightLevel, 10, 64)
	if err != nil {
		fmt.Println("right level is not a number")
		//return
	}
	return &appilcation.CreateBranch{"CreateBranch", branchName, uint(rightLevelInt)}
}

func (c *CLI) changeBranch(reader *bufio.Reader) boilerplate.UsecaseTO {
	branchName := ask("Enter branch name")
	//fmt.Println(c.Application.ChangeBranch(branchName))
	return &appilcation.ChangeBranch{"ChangeBranch", branchName}
}

func (c *CLI) commit(reader *bufio.Reader) boilerplate.UsecaseTO {
	commitName := ask("Enter commit name")
	//fmt.Println(c.Application.Commit(commitName))
	return &appilcation.Commit{"Commit", commitName}
}

func (c *CLI) createRemote(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	privateKey := ask("Enter private key")
	email := ask("Enter your Email address")

	//fmt.Println(c.Application.CreateRemote(url, privateKey))
	return &appilcation.CreateRemote{"CreateRemote", url, privateKey, email}
}

func (c *CLI) getUsers(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	privateKey := ask("Enter private key")
	address := ask("Enter contract address")

	//fmt.Println(c.Application.GetAllUser(url, address, privateKey))
	return &appilcation.GetAllUser{"GetAllUser", url, address, privateKey}
}

func (c *CLI) addUser(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	privateKey := ask("Enter private key")
	address := ask("Enter contract address")
	email := ask("Enter new user email")
	publicKey := ask("Enter new user public key")
	rightLevel := ask("Enter new user right level")

	rightLevelInt, err := strconv.ParseInt(rightLevel, 10, 64)
	if err != nil {
		fmt.Println("right level is not a number")
		panic("right level is not a number")
		//return
	}

	//fmt.Println(c.Application.CreateUser(url, address, privateKey, email, publicKey, rightLevelInt))
	return &appilcation.CreateUser{"CreateUser", url, address, privateKey, email, publicKey, rightLevelInt}
}

func (c *CLI) updateUser(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	privateKey := ask("Enter private key")
	address := ask("Enter contract address")
	email := ask("Enter new user email")
	publicKey := ask("Enter user public key")
	rightLevel := ask("Enter new user right level")

	rightLevelInt, err := strconv.ParseInt(rightLevel, 10, 64)
	if err != nil {
		fmt.Println("right level is not a number")
		//return
	}

	return &appilcation.UpdateUser{"UpdateUser", url, address, privateKey, email, publicKey, rightLevelInt}
}

func (c *CLI) deleteUser(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	address := ask("Enter contract address")
	privateKey := ask("Enter private Key")
	publicKeyToDelete := ask("Enter key of user to delete")

	//fmt.Println(c.Application.DeleteUser(url, address, privateKey, publicKeyToDelete))
	return &appilcation.DeleteUser{"DeleteUser", url, address, privateKey, publicKeyToDelete}
}

func (c *CLI) merge() boilerplate.UsecaseTO {
	branchToMerge := ask("Branch to merge into current Branch")
	//fmt.Println(c.Application.Merge(branchToMerge))
	return &appilcation.Merge{"Merge", branchToMerge}
}

func (c *CLI) push(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	address := ask("Enter contract address")
	privateKey := ask("Enter private Key")
	//fmt.Println(c.Application.Push(url, address, privateKey))
	return &appilcation.Push{"Push", url, address, privateKey}
}

func (c *CLI) pull(reader *bufio.Reader) boilerplate.UsecaseTO {
	url := ask("Enter url")
	address := ask("Enter contract address")
	privateKey := ask("Enter private Key")
	//branchName := ask("Enter branch name")
	//fmt.Println(c.Application.Pull(url, address, privateKey, branchName))
	return &appilcation.Pull{"Pull", url, address, privateKey, ""}
}

func (c *CLI) getRemoteBranches() boilerplate.UsecaseTO {
	url := ask("Enter url")
	address := ask("Enter contract address")
	privateKey := ask("Enter private Key")
	//fmt.Println(c.Application.GetRemoteBranches(url, address, privateKey))
	return &appilcation.GetRemoteBranches{"GetRemoteBranches", url, address, privateKey}
}

func (c *CLI) config(reader *bufio.Reader) {
	fmt.Println("Enter your email address")
	//email, _ := reader.ReadString('\n')
	fmt.Println("Enter your public key")
	//	publicKey, _ := reader.ReadString('\n')
	//res := usecase_legacy.UpdateUser(email, publicKey)
	//	fmt.Println(res)
}
