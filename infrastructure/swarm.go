package infrastructure

import (
	"ethVC/boilerplate"
	"fmt"
	swarm "github.com/ethereum/go-ethereum/swarm/api/client"
	"io/ioutil"
	"strings"
)

type RemoteDataConnector struct {
}

func (RemoteDataConnector) Connect(url string) boilerplate.RemoteData {
	_ = url
	return NewSwarm()
}

type Swarm struct {
	client *swarm.Client
}

func NewSwarm() boilerplate.RemoteData {
	client := swarm.NewClient("http://127.0.0.1:8500")
	return &Swarm{client}
}

func (s *Swarm) UploadString(content string) (string, error) {
	reader := strings.NewReader(content)
	foo, err := s.client.UploadRaw(reader, int64(len(content)), false)
	return foo, err
}

func (s *Swarm) UploadFile(path string) (string, error) {
	file, err := swarm.Open(path)
	if err != nil {
		return "", err
	}

	manifestHash, err := s.client.Upload(file, "", false)
	if err != nil {
		return "", err
	}
	return manifestHash, nil
}

func (s *Swarm) GetFile(hash string) (string, error) {
	fmt.Println(hash)
	file, _, err := s.client.DownloadRaw(hash)
	if err != nil {
		fmt.Println("Error on fetching file")
		return "", err
	}

	content, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}
	fmt.Println(content)
	return string(content), nil
}

func (s *Swarm) GetFileAsBytes(hash string) ([]byte, error) {
	file, _, err := s.client.DownloadRaw(hash)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(file)
}

/*
func (s *Swarm)AddDirectory(path string) (string, error) {
	return "", nil
}

func (s *Swarm)GetDirectory(hash string) (string, error) {
	return "", nil
}
*/
