package test_util

type SpyCounter interface {
	Reset()
	Increment()
	Get() uint
}

type SpyCounterImpl struct {
	Called uint
}

func (s *SpyCounterImpl) Reset() {
	s.Called = 0
}

func (s *SpyCounterImpl) Increment() {
	s.Called++
}

func (s *SpyCounterImpl) Get() uint {
	return s.Called
}
