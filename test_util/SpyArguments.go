package test_util

type SpyArguments interface {
	AddArguments(args ...interface{})
	GetArgumentAt(callIndex int, argumentIndex int) interface{}
	GetNumberOfCalled() int
	GetNumberOfArguments(callIndex int) int
}

type SpyArgumentsImpl struct {
	args [][]interface{}
}

func NewSpyArguments() SpyArguments {
	var args [][]interface{}
	return &SpyArgumentsImpl{args}
}

func (s *SpyArgumentsImpl) AddArguments(args ...interface{}) {
	s.args = append(s.args, args)
}

func (s *SpyArgumentsImpl) GetArgumentAt(callIndex int, argumentIndex int) interface{} {
	return s.args[callIndex][argumentIndex]
}

func (s *SpyArgumentsImpl) GetNumberOfCalled() int {
	return len(s.args)
}

func (s *SpyArgumentsImpl) GetNumberOfArguments(callIndex int) int {
	return len(s.args[callIndex])
}
