package test_util

type StrSpy interface {
	CallNthReturnStr(string)
	ReturnEveryTimeStr(string)
	Return() string
	Reset()
	CalledWith(parameter string)
	GetNumberOfCalled() int
	GetCalledWith() []string
}

type StrSpyImpl struct {
	called               int
	everyTime            bool
	everyTimeReturnValue string
	returnValues         []string
	parameters           []string
}

func (d *StrSpyImpl) CallNthReturnStr(ret string) {
	d.returnValues = append(d.returnValues, ret)
}

func (d *StrSpyImpl) ReturnEveryTimeStr(ret string) {
	d.everyTime = true
	d.everyTimeReturnValue = ret
}

func (d *StrSpyImpl) Reset() {
	d.everyTime = false
	d.everyTimeReturnValue = ""
	d.returnValues = []string{}
	d.called = 0
}

func (d *StrSpyImpl) Return() string {
	defer func() { d.called++ }()
	if d.everyTime {
		return d.everyTimeReturnValue
	} else {
		if len(d.returnValues) > d.called {
			return d.returnValues[d.called]
		}
	}
	return ""
}

func (d *StrSpyImpl) CalledWith(parameter string) {
	d.parameters = append(d.parameters, parameter)
}

func (d *StrSpyImpl) GetNumberOfCalled() int {
	return d.called
}

func (d *StrSpyImpl) GetCalledWith() []string {
	return d.parameters
}

func NewStrSpy() StrSpy {
	return &StrSpyImpl{0, false, "", []string{}, []string{}}
}
