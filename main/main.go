package main

import (
	"ethVC/appilcation"
	"ethVC/boilerplate"
	"ethVC/infrastructure"
)

func main() {
	ui := &infrastructure.CLI{}
	persist := infrastructure.NewJson()
	fileIO := infrastructure.NewFileIO()
	remoteMetaConnector := infrastructure.RemoteMetaConnectorImpl{}
	remoteDataConnector := infrastructure.RemoteDataConnector{}

	infrastructureManager := boilerplate.NewInfrastructureManagerImpl(ui, persist, fileIO, remoteMetaConnector, remoteDataConnector)

	application := appilcation.ApplicationImpl{infrastructureManager}
	application.Run()
}
