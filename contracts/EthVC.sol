pragma solidity ^0.4.0;

// TODO check index before accessing array <- not necessary nothing bad happens, but no feedback is provided
// TODO store name of repo in contract
contract EthVC {

    // ------------------- User Management -------------------
    uint NO_RIGHT = 0;
    uint DEVELOPER = 1;
    uint MAINTAINER = 2;
    uint OWNER = 3;

    uint USER_CREATED_FAILED = 0;
    uint USER_CREATED_SUCCESS = 1;
    uint USER_UPDATED_FAILED = 2;
    uint USER_UPDATED_SUCCESS = 3;
    uint USER_DELETED_FAILED = 4;
    uint USER_DELETED_SUCCESS = 5;

    uint REPOSITORY_CREATED = 6;

    uint BRANCH_CREATED_SUCCESS = 7;
    uint BRANCH_CREATED_ERROR_ALREADY_EXISTS = 8;
    uint BRANCH_CREATED_ERROR_RIGHT_LEVEL_TO_LOW = 9;

    uint ADD_INDEX_FILE_SUCCESS = 10;
    uint ADD_INDEX_FILE_FAILED = 11;
    uint ADD_INDEX_FILE_BRANCH_DOES_NOT_EXIST = 12;
    uint ADD_INDEX_FILE_RIGHT_LEVEL_TO_LOW = 13;
    uint ADD_INDEX_FILE_TO_LOW_REVISION = 14;

    struct UserData {
        bytes32 email;
        uint rightLevel;
        uint index;
    }

    event UserEvent(address userAdress , uint successCode);
    event VCEvent(bytes32 branchName, uint successCode);

    address[] private authorizedUserIndex;
    mapping(address => UserData) private userDetails; // child contracts are unable to override mapping (private) to ensure integrity

    struct Branch {
        uint rightLevel;
        bytes32 name;
        uint creationDateUnix;
        uint revision;
        uint index;
        bytes32 indexFilesUpper; // one file per commit
        bytes32 indexFilesLower; // one file per commit
    }

    bytes32[] branchNames;
    mapping(bytes32 => Branch) private branches; // name => revision

    // Create initial user with highest rights.
    constructor(bytes32 email)  public {
        addUser(msg.sender, email, OWNER);
        emit UserEvent(msg.sender, REPOSITORY_CREATED); // Does not work properly ?
    }

    // ------------------- User Management -------------------

    function addAuthorizedUser(address newUserAddress, bytes32 email, uint rightLevel) external {
        if (isUserKnown(msg.sender) && getUserRight(msg.sender) >= 3 && !isUserKnown(newUserAddress)) {
            addUser(newUserAddress, email, rightLevel);
            emit UserEvent(newUserAddress, USER_CREATED_SUCCESS);
        } else {
            emit UserEvent(newUserAddress, USER_CREATED_FAILED);
        }
    }

    function deleteUser(address userToDelete) external {
        if (isUserKnown(msg.sender) && getUserRight(msg.sender) >= 3 && isUserKnown(userToDelete)) {
            uint rowToDelete = userDetails[userToDelete].index;
            userDetails[userToDelete].email = '';
            userDetails[userToDelete].rightLevel = 0;
            userDetails[userToDelete].index = 0;
            address keyToMove = authorizedUserIndex[authorizedUserIndex.length-1];
            authorizedUserIndex[rowToDelete] = keyToMove;
            userDetails[keyToMove].index = rowToDelete;
            authorizedUserIndex.length--;
            emit UserEvent(userToDelete, USER_DELETED_SUCCESS);
        } else {
            emit UserEvent(userToDelete, USER_DELETED_FAILED);
        }
    }

    function updateUser(address userToUpdate, bytes32 newEmail, uint newRightLevel) external {
        if (isUserKnown(msg.sender) && getUserRight(msg.sender) >= 3) {
            userDetails[userToUpdate].email = newEmail;
            userDetails[userToUpdate].rightLevel = newRightLevel;
            emit UserEvent(userToUpdate, USER_UPDATED_SUCCESS);

        } else {
            emit UserEvent(userToUpdate, USER_DELETED_FAILED);
        }
    }

    function addUser(address newUserAddress, bytes32 email, uint rightLevel) internal {
        userDetails[newUserAddress].index = authorizedUserIndex.push(newUserAddress)-1; // push returns index.
        userDetails[newUserAddress].email = email;
        userDetails[newUserAddress].rightLevel = rightLevel;
    }

    function isUserKnown(address userAddress) public constant returns (bool isKnown) {
        if (authorizedUserIndex.length == 0) return false; // TODO remove. It is never the case.
        return (authorizedUserIndex[userDetails[userAddress].index] == userAddress);
    }

    function getUserRight(address userAddress) private constant returns (uint) {
        return userDetails[userAddress].rightLevel;
    }

    function getUserEmail(address userAddress) private constant returns (bytes32) {
        return userDetails[userAddress].email;
    }

    function getUserCount() public constant returns(uint count) {
        return authorizedUserIndex.length;
    }

    function getUserAtIndex(uint index) public constant returns(address userAddress){
        return authorizedUserIndex[index];
    }

    function getUserEmailAtIndex(uint index) public constant returns(bytes32 email){
        address userAddress =  authorizedUserIndex[index];
        return userDetails[userAddress].email;
    }

    function getUserRightLevelAtIndex(uint index) public constant returns(uint rightLevel){
        address userAddress =  authorizedUserIndex[index];
        return userDetails[userAddress].rightLevel;
    }

    // ------------------- Branch Management -------------------

    function addBranch(uint rightLevel,bytes32 name, uint creationDateUnix) external {
        if (!isUserKnown(msg.sender) || getUserRight(msg.sender) < 2) {
            emit VCEvent(name, BRANCH_CREATED_ERROR_RIGHT_LEVEL_TO_LOW);
        }

        if (branches[name].revision == 0) {
            branches[name].index = branchNames.push(name)-1;
            branches[name].name = name;
            branches[name].rightLevel = rightLevel;
            branches[name].revision = 1;
            branches[name].creationDateUnix = creationDateUnix;
            emit VCEvent(name, BRANCH_CREATED_SUCCESS);
        } else {
            emit VCEvent(name, BRANCH_CREATED_ERROR_ALREADY_EXISTS);
        }
    }

    function addCommit(bytes32 branchName, bytes32 indexFileUpper, bytes32 indexFileLower, uint revision) external {
        if (branches[branchName].revision == 0) {
            emit VCEvent(branchName, ADD_INDEX_FILE_BRANCH_DOES_NOT_EXIST);
        }

        if (branches[branchName].revision > revision) {
            emit VCEvent(branchName, ADD_INDEX_FILE_TO_LOW_REVISION);
        }

        if (isUserKnown(msg.sender) && getUserRight(msg.sender) >= branches[branchName].rightLevel) {
            branches[branchName].indexFilesUpper = indexFileUpper;
            branches[branchName].indexFilesLower = indexFileLower;
            branches[branchName].revision++;
            emit VCEvent(branchName, ADD_INDEX_FILE_SUCCESS);
        } else {
            emit VCEvent(branchName, ADD_INDEX_FILE_RIGHT_LEVEL_TO_LOW);
        }
    }

    function getBranchNames() external constant returns (bytes32[]) {
        return branchNames;
    }

    function getBranchByName(bytes32 branchName) external constant returns (bytes32, uint, uint, bytes32, bytes32, uint){
        return (branches[branchName].name, branches[branchName].creationDateUnix,
        branches[branchName].rightLevel, branches[branchName].indexFilesUpper, branches[branchName].indexFilesLower, branches[branchName].revision);
    }

}
