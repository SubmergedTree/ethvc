package domain

import (
	"bufio"
	"ethVC/boilerplate"
	"fmt"
	"log"
	"strings"
)

type FileFactoryImpl struct {
	RemoteData    boilerplate.RemoteData
	UserInterface boilerplate.UserInterface
	FileIO        boilerplate.FileIO
}

type FileImpl struct {
	Content string
	Path    string

	fileIO        boilerplate.FileIO
	userInterface boilerplate.UserInterface
	remoteData    boilerplate.RemoteData
}

type WhichFile uint8

const (
	FirstFile  = 0
	SecondFile = 1
)

func (f *FileFactoryImpl) NewFromPath(path string) (File, error) {
	content, err := f.FileIO.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return NewFileImpl(content, path, f.FileIO, f.UserInterface, f.RemoteData), nil
}

func (f *FileFactoryImpl) NewFromData(content string, path string) File {
	return NewFileImpl(content, path, f.FileIO, f.UserInterface, f.RemoteData)
}

func (f *FileFactoryImpl) NewFromRemote(address string, path string) (File, error) {

	content, err := f.RemoteData.GetFile(address)
	if err != nil {
		return nil, err
	}
	return NewFileImpl(content, path, f.FileIO, f.UserInterface, f.RemoteData), nil
}

func NewFileImpl(content, path string, fileIO boilerplate.FileIO, userInterface boilerplate.UserInterface, remoteData boilerplate.RemoteData) File {
	return &FileImpl{content, path, fileIO, userInterface, remoteData}
}

func (file *FileImpl) GetContentLineByLine() (result []string) {
	scanner := bufio.NewScanner(strings.NewReader(file.Content))
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return result
}

func (first *FileImpl) MergeWith(second File) (File, error) {
	if first.Path != second.GetPath() {
		return nil, fmt.Errorf("merging two different files is not allowed")
	}

	var content strings.Builder

	contentFirst := first.GetContentLineByLine()
	contentSecond := second.GetContentLineByLine()

	var absLen int

	if len(contentFirst) > len(contentSecond) {
		absLen = len(contentFirst)
	} else {
		absLen = len(contentSecond)
	}

	for index := 0; index < absLen; index++ {
		content.WriteString(first.mergeLine(index, contentFirst, contentSecond))
	}

	strRep := content.String()
	strRep = strRep[:len(strRep)-1]

	//return NewFromData(strRep, first.Path), nil
	return &FileImpl{strRep, first.Path,
		first.fileIO, first.userInterface, first.remoteData}, nil
}

func (file *FileImpl) Write() error {
	return file.fileIO.NestedWrite(file.Path, file.Content)
}

func (file *FileImpl) mergeLine(index int, contentFirst, contentSecond []string) string {
	firstLine := accessByIndex(index, contentFirst)
	secondLine := accessByIndex(index, contentSecond)
	file.userInterface.Show("_____________-")
	if firstLine == nil {
		return *secondLine + "\n"
	} else if secondLine == nil {
		return *firstLine + "\n"
	}

	if *firstLine == *secondLine {
		return *firstLine + "\n"
	}

	which := file.askWhichLine(*firstLine, *secondLine)

	if which == FirstFile {
		return *firstLine + "\n"
	} else {
		return *secondLine + "\n"
	}
}

func accessByIndex(index int, content []string) *string {
	if index >= len(content) {
		return nil
	}
	return &content[index]
}

func (file *FileImpl) askWhichLine(first string, second string) WhichFile {
	for true {
		answer := file.userInterface.Ask(first + " \n  OR  \n" + second)
		if answer == "1" {
			return FirstFile
		} else if answer == "2" {
			return SecondFile
		}
	}
	return FirstFile // is never the case. (at least should be never the case)
}

func (file *FileImpl) GetPath() string {
	return file.Path
}

func (file *FileImpl) StoreRemotely() (string, error) {
	address, err := file.remoteData.UploadString(file.Content)
	if err != nil {
		return "", err
	}
	return address, nil
}
