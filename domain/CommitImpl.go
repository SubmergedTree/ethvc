package domain

import (
	"encoding/json"
	"ethVC/boilerplate"
	"fmt"
)

type CommitFactoryImpl struct {
	RemoteData  boilerplate.RemoteData
	FileFactory FileFactory
	FileIO      boilerplate.FileIO
}

type CommitImpl struct {
	Name             string
	Files            []File
	CreationDateUnix uint
	Previous         Commit
	next             Commit // double linking makes saving much easier. next refers to the newer commit.
	Author           string

	remoteData boilerplate.RemoteData
}

type remoteSnapshot struct {
	Address string `json:"address"`
	Path    string `json:"path"`
}

type commitRemoteRepresentation struct {
	Snapshot         []remoteSnapshot `json:"snapshot"` // snapshot is basically an array of file addresses
	Author           string           `json:"author"`
	Name             string           `json:"name"`
	CreationDateUnix int              `json:"creationDateUnix"`
	Previous         string           `json:"previous"`
}

func (c *CommitFactoryImpl) NewCommitImpl(name string, creationDateUnix uint, author string) Commit {
	var commits []File
	return &CommitImpl{name, commits, creationDateUnix,
		nil, nil, author, c.RemoteData}
}

func (c *CommitFactoryImpl) NewRemoteCommitImpl(address string) (Commit, error) {
	rd := c.RemoteData
	fmt.Println("fetch last commit")
	fmt.Println(address)

	remoteCommit, err := c.createRemoteCommitRepresentation(rd, address)
	if err != nil {
		return nil, err
	}

	files, err := c.createFilesFromRemote(remoteCommit.Snapshot)
	if err != nil {
		return nil, err
	}

	commit := &CommitImpl{remoteCommit.Name, files, uint(remoteCommit.CreationDateUnix),
		nil, nil, remoteCommit.Author, rd}

	var toLink []Commit
	prevAddress := remoteCommit.Previous

	for prevAddress != "" {

		remoteCommit, err := c.createRemoteCommitRepresentation(rd, prevAddress)
		if err != nil {
			return nil, err
		}

		files, err := c.createFilesFromRemote(remoteCommit.Snapshot)
		if err != nil {
			return nil, err
		}

		toLink = append(toLink, &CommitImpl{remoteCommit.Name, files, uint(remoteCommit.CreationDateUnix),
			nil, nil, remoteCommit.Author, rd})

		prevAddress = remoteCommit.Previous
	}

	for idx, l := range toLink {
		if idx == 0 {
			commit.Previous = l
			commit.GetPrevious().SetPrevious(nil)
			//l.SetNext(commit)
			l.SetNext(nil)
			if idx != len(toLink)-1 {
				l.SetPrevious(toLink[idx+1])
			}
		} else if idx == len(toLink)-1 {
			l.SetNext(toLink[idx-1])
		} else {
			l.SetPrevious(toLink[idx+1])
			l.SetNext(toLink[idx-1])

		}
	}
	return commit, nil
}

func (c *CommitFactoryImpl) NewCommitFromAttributes(attributes map[string]interface{}) (Commit, error) {
	var commits []Commit

	name := attributes["Name"].(string)
	creationDate := attributes["CreationDateUnix"].(float64)
	author := attributes["Author"].(string)
	commit := c.NewCommitImpl(name, uint(creationDate), author)
	filesAttres := attributes["Files"].([]interface{})
	for _, attr := range filesAttres {
		fileAttr := attr.(map[string]interface{})
		content := fileAttr["Content"].(string)
		path := fileAttr["Path"].(string)
		commit.AddFile(c.FileFactory.NewFromData(content, path))
	}
	commits = append(commits, commit)

	previous, ok := attributes["Previous"].(map[string]interface{})
	for {
		if ok == true {
			creationDate := previous["CreationDateUnix"].(float64)
			author := previous["Author"].(string)
			name := previous["Name"].(string)

			commit = c.NewCommitImpl(name, uint(creationDate), author)
			filesAttres := previous["Files"].([]interface{})
			for _, attr := range filesAttres {
				fileAttr := attr.(map[string]interface{})
				content := fileAttr["Content"].(string)
				path := fileAttr["Path"].(string)
				commit.AddFile(c.FileFactory.NewFromData(content, path))
			}
			commits = append(commits, commit)
			previous, ok = previous["Previous"].(map[string]interface{})
		} else {
			break
		}
	}

	//fmt.Println(len(commits))
	//fmt.Println(commits)
	commit = commits[0]

	for idx, l := range commits {
		if idx == 0 {
			commit.SetPrevious(l)
			commit.GetPrevious().SetPrevious(nil)
			l.SetNext(nil)
			if idx != len(commits)-1 {
				l.SetPrevious(commits[idx+1])
			}
		} else if idx == len(commits)-1 {
			l.SetNext(commits[idx-1])
			//l.SetPrevious(nil)
		} else {
			l.SetPrevious(commits[idx+1])
			l.SetNext(commits[idx-1])

		}
	}

	//	fmt.Println(commit.GetName())
	//	fmt.Println(commit.GetNext().GetName())
	/*
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetNext().GetNext().GetNext().GetNext().GetNext())


		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetNext().GetNext().GetNext().GetNext().GetName())
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetNext().GetNext().GetNext().GetName())
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetNext().GetNext().GetName())
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetNext().GetName())
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetNext().GetName())
		fmt.Println(commit.GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetPrevious().GetName())
	*/

	return commit, nil
}

func (c *CommitFactoryImpl) NewCommitFromPath(name string, creationDateUnix uint, author string, path string) (Commit, error) {
	commit := c.NewCommitImpl(name, creationDateUnix, author)
	paths, err := c.FileIO.GetAllFilePaths(path)
	if err != nil {
		return nil, err
	}
	for _, p := range paths {
		file, err := c.FileFactory.NewFromPath(p)
		if err != nil {
			return nil, err
		}
		commit.AddFile(file)
	}
	return commit, nil
}

func (CommitFactoryImpl) createRemoteCommitRepresentation(rd boilerplate.RemoteData,
	address string) (*commitRemoteRepresentation, error) {
	content, err := rd.GetFile(address)
	if err != nil {
		return nil, err
	}
	fmt.Println(content)
	var remoteCommit commitRemoteRepresentation

	if err := json.Unmarshal([]byte(content), &remoteCommit); err != nil {
		return nil, err
	}
	return &remoteCommit, nil
}

func (c *CommitFactoryImpl) createFilesFromRemote(snapshot []remoteSnapshot) ([]File, error) {
	var files []File

	for _, snapshot := range snapshot {
		file, err := c.FileFactory.NewFromRemote(snapshot.Address, snapshot.Path)
		if err != nil {
			return nil, err
		}
		files = append(files, file)
	}
	return files, nil
}

func (c *CommitFactoryImpl) NewMergeCommit(creationDateUnix uint, author string, commit1, commit2 Commit) (Commit, error) {
	var files []File

	for _, c1File := range commit1.GetFiles() {
		corresponding := getFileIfExists(commit2.GetFiles(), c1File.GetPath())
		if corresponding == nil {
			files = append(files, c1File)
		} else {
			merged, err := c1File.MergeWith(corresponding)
			if err != nil {
				return nil, err
			}
			files = append(files, merged)
		}
	}

	for _, c2File := range commit2.GetFiles() {
		if !doesFileExist(files, c2File.GetPath()) {
			files = append(files, c2File)
		}
	}

	commit := &CommitImpl{"Merge Commit", files, creationDateUnix,
		nil, nil, author, c.RemoteData}

	return commit, nil
}

func getFileIfExists(files []File, path string) File {
	for _, f := range files {
		if f.GetPath() == path {
			return f
		}
	}
	return nil
}

func doesFileExist(files []File, path string) bool {
	for _, f := range files {
		if f.GetPath() == path {
			return true
		}
	}
	return false
}

func (commit *CommitImpl) AddFile(file File) {
	commit.Files = append(commit.Files, file)
}

func (commit *CommitImpl) GetPrevious() Commit {
	return commit.Previous
}

func (commit *CommitImpl) SetPrevious(prev Commit) {
	commit.Previous = prev
}

func (commit *CommitImpl) GetNext() Commit {
	return commit.next
}

func (commit *CommitImpl) SetNext(next Commit) {
	commit.next = next
}

func (commit *CommitImpl) SetActive() {
	for _, file := range commit.Files {
		file.Write() // TODO error check
	}
}

func (commit *CommitImpl) GetFiles() []File {
	return commit.Files
}

func (commit *CommitImpl) SaveRemotely() (string, error) {
	nextCommit := commit.getOldestCommit()
	prevAddress := ""

	for {
		//	fmt.Printf("Saving commit %s to swarm \n", nextCommit.Name)
		snapshots, err := storeFilesRemotely(nextCommit)
		if err != nil {
			return "", err
		}

		remoteJson, err := getRemoteRepresentationAsJson(nextCommit, prevAddress, snapshots)
		if err != nil {
			return "", err
		}

		prevAddress, err = commit.remoteData.UploadString(remoteJson)
		if err != nil {
			return "", err
		}
		//	fmt.Println(prevAddress)

		if nextCommit.next != nil {
			nextCommit = nextCommit.next.(*CommitImpl)
		} else {
			//fmt.Println("break")
			break
		}
	}
	//	fmt.Println("return address :")
	//	fmt.Println(prevAddress)
	return prevAddress, nil
}

func (commit *CommitImpl) getOldestCommit() *CommitImpl {
	latest := commit
	for latest.Previous != nil {
		latest = latest.Previous.(*CommitImpl) // it would be better to Link with *CommitImpl instead of Commit
	}
	return latest
}

func storeFilesRemotely(commit *CommitImpl) ([]remoteSnapshot, error) {
	var snapshots []remoteSnapshot

	for _, file := range commit.Files {
		address, err := file.StoreRemotely()
		if err != nil {
			return nil, err
		}
		snapshots = append(snapshots, remoteSnapshot{address, file.GetPath()})
	}
	return snapshots, nil
}

func getRemoteRepresentationAsJson(commit *CommitImpl, prevAddress string,
	snapshots []remoteSnapshot) (string, error) {
	crp := &commitRemoteRepresentation{snapshots,
		commit.Author, commit.Name, int(commit.CreationDateUnix),
		prevAddress}

	jsonRemote, err := json.Marshal(crp)
	if err != nil {
		return "", err
	}

	return string(jsonRemote[:]), nil
}

func (commit *CommitImpl) GetName() string {
	return commit.Name
}

func (commit *CommitImpl) GetCreationDate() uint {
	return commit.CreationDateUnix
}
