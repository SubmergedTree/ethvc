package domain

type Branch interface {
	AddCommit(Commit)
	CreateCommit(name string, creationDateUnix uint, author string, topPath string) error
	SetActive()
	SaveRemotely() (isLocalBranchUpToDate bool, err error)
	UpdateFromRemote(creationDateUnix uint, author string) (bool, error)
	Merge(mergeInto Branch, creationDateUnix uint, author string) error
	GetLastCommit() Commit
	GetName() string
}

type BranchFactory interface {
	NewBranchImpl(accessRight uint, name string, creationDateUnix uint) Branch
	NewRemoteBranchImpl(branchName string) (Branch, error)
	NewBranchWithLastCommit(accessRight uint, name string, creationDateUnix uint, lastCommit Commit) Branch // TODO test
	NewBranchFromAttributes(interface{}) (Branch, error)
}
