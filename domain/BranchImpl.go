package domain

import (
	"ethVC/boilerplate"
)

type BranchImplFactory struct {
	RemoteMeta    boilerplate.RemoteMeta
	CommitFactory CommitFactory
}

type BranchImpl struct {
	AccessRight      uint
	Name             string
	CreationDateUnix uint
	LastCommit       Commit
	Revision         uint

	remoteMeta    boilerplate.RemoteMeta
	commitFactory CommitFactory
}

func (b *BranchImplFactory) NewBranchImpl(accessRight uint, name string, creationDateUnix uint) Branch {
	//return &BranchImpl{accessRight, name, creationDateUnix,
	//		nil, 0, b.RemoteMeta, b.CommitFactory}
	return NewBranchImpl(accessRight, name, creationDateUnix, nil, 0, b.RemoteMeta, b.CommitFactory)
}

//TODO Last commit
func (b *BranchImplFactory) NewRemoteBranchImpl(branchName string) (Branch, error) {
	branches, err := b.RemoteMeta.GetBranches()
	if err != nil {
		return nil, err
	}

	for _, branch := range branches {
		if branch.Name == branchName {
			if branch.LastCommitAddress != "" {
				commit, err := b.CommitFactory.NewRemoteCommitImpl(branch.LastCommitAddress)
				if err != nil {
					return nil, err
				}
				return &BranchImpl{uint(branch.AccessRights), branch.Name, uint(branch.CreationDateUnix),
					commit, uint(branch.Revision), b.RemoteMeta, b.CommitFactory}, err
			}
			return &BranchImpl{uint(branch.AccessRights), branch.Name, uint(branch.CreationDateUnix),
				nil, uint(branch.Revision), b.RemoteMeta, b.CommitFactory}, err
		}
	}
	return nil, nil
}

func (b *BranchImplFactory) NewBranchFromAttributes(i interface{}) (Branch, error) {
	attributes := i.(map[string]interface{})
	accessRight := attributes["AccessRight"].(float64)
	name := attributes["Name"].(string)
	creationDate := attributes["CreationDateUnix"].(float64)
	lastCommitAttributes, ok := attributes["LastCommit"].(map[string]interface{})
	var lastCommit Commit
	if ok == false {
		lastCommit = nil
	} else {
		c, err := b.CommitFactory.NewCommitFromAttributes(lastCommitAttributes)
		if err != nil {
			return nil, err
		}
		lastCommit = c
	}
	revision := attributes["Revision"].(float64)

	return &BranchImpl{uint(accessRight), name, uint(creationDate), lastCommit,
		uint(revision), b.RemoteMeta, b.CommitFactory}, nil
}

func (b *BranchImplFactory) NewBranchWithLastCommit(accessRight uint, name string, creationDateUnix uint, lastCommit Commit) Branch {
	return NewBranchImpl(accessRight, name, creationDateUnix, lastCommit, 0, b.RemoteMeta, b.CommitFactory)
}

func NewBranchImpl(accessRight uint, name string, creationDateUnix uint, lastCommit Commit, revision int,
	remoteMeta boilerplate.RemoteMeta, commitFactory CommitFactory) Branch {
	return &BranchImpl{uint(accessRight), name, uint(creationDateUnix),
		lastCommit, uint(revision), remoteMeta, commitFactory}
}

func (b *BranchImpl) GetName() string {
	return b.Name
}

func (b *BranchImpl) AddCommit(commit Commit) {
	if b.LastCommit == nil {
		b.LastCommit = commit
	} else {
		b.LastCommit.SetNext(commit)
		commit.SetPrevious(b.LastCommit)
		b.LastCommit = commit
	}
	b.SetActive() // is this call here correct ?
}

func (b *BranchImpl) CreateCommit(name string, creationDateUnix uint, author string, topPath string) error {
	commit, err := b.commitFactory.NewCommitFromPath(name, creationDateUnix, author, topPath)
	if err != nil {
		return err
	}
	b.AddCommit(commit)

	/*	if b.LastCommit == nil {
			b.LastCommit = commit
		} else {
			b.LastCommit.SetNext(commit)
			commit.SetPrevious(b.LastCommit)
			b.LastCommit = commit
		}
		b.SetActive() // is this call here correct ?*/
	return nil
}

func (b *BranchImpl) SetActive() {
	if b.LastCommit != nil {
		b.LastCommit.SetActive()
	}
}

func (b *BranchImpl) Merge(mergeInto Branch, creationDateUnix uint, author string) error {
	mergeCommit, err := b.commitFactory.NewMergeCommit(creationDateUnix, author, b.LastCommit, mergeInto.GetLastCommit())
	if err != nil {
		return err
	}
	b.AddCommit(mergeCommit)
	return nil
}

func (b *BranchImpl) GetLastCommit() Commit {
	return b.LastCommit
}

func (b *BranchImpl) SaveRemotely() (isLocalBranchUpToDate bool, err error) {
	doesExists, metaBranchPOD, err := b.doesBranchExist()
	if err != nil {
		return false, err
	}
	//localCommits := b.getAllLocalCommits()
	//	fmt.Printf("Does exists %b", doesExists)
	if doesExists {
		if metaBranchPOD.Revision > int64(b.Revision) || b.LastCommit == nil {
			return false, nil
		}

		address, err := b.LastCommit.SaveRemotely()
		if err != nil {
			return true, err
		}
		err = b.remoteMeta.AddCommit(address, b.Name, int64(b.Revision))
		if err != nil {
			return true, err
		}
		b.Revision++

	} else {
		err := b.remoteMeta.CreateBranch(b.Name, int64(b.CreationDateUnix), int64(b.AccessRight))
		if err != nil {
			return true, err
		}
		address, err := b.LastCommit.SaveRemotely()
		err = b.remoteMeta.AddCommit(address, b.Name, int64(b.Revision))
		if err != nil {
			return true, err
		}
		b.Revision++
	}
	return true, nil
}

func (b *BranchImpl) UpdateFromRemote(creationDateUnix uint, author string) (bool, error) {
	doesExists, metaBranchPOD, err := b.doesBranchExist()
	if err != nil {
		return false, err
	}
	//fmt.Println(doesExists)
	//fmt.Println(metaBranchPOD.Name)
	//fmt.Println(metaBranchPOD.LastCommitAddress)

	if doesExists {
		lastRemoteCommit, err := b.commitFactory.NewRemoteCommitImpl(metaBranchPOD.LastCommitAddress)
		if err != nil {
			return true, err
		}
		mergeCommit, err := b.commitFactory.NewMergeCommit(creationDateUnix, author, lastRemoteCommit, b.LastCommit)
		if err != nil {
			return true, err
		}
		b.AddCommit(mergeCommit)
		b.Revision = uint(metaBranchPOD.Revision)
		return true, nil
	}
	return false, nil
}

func (b *BranchImpl) doesBranchExist() (bool, *boilerplate.BranchMetaPOD, error) {
	metaPods, err := b.remoteMeta.GetBranches()
	if err != nil {
		return false, nil, err
	}
	for _, metaPod := range metaPods {
		if metaPod.Name == b.Name {
			return true, metaPod, nil
		}
	}
	return false, nil, nil
}

/*
func (b *BranchImpl)getAllLocalCommits() []Commit{
	var commits []Commit
	commitPtr := b.LastCommit
	for {
		commits = append(commits, commitPtr)
		commitPtr = commitPtr.GetPrevious()
	}
	return commits
}

func getNotRemoteRepresentedCommits(localCommits []Commit, remoteCreationDate uint) []Commit {
	sort.Slice(localCommits, func(i, j int) bool {
		return localCommits[i].GetCreationDate() < localCommits[j].GetCreationDate()
	})

	var commits []Commit
	for _, commit := range localCommits {
		if commit.GetCreationDate() >= remoteCreationDate {
			commits = append(commits, commit)
		}
	}
	return commits
}*/
