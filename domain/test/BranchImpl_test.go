package test

import (
	"ethVC/domain"
	"ethVC/domain/test/mock"
	"ethVC/test_util"
	"reflect"
	"testing"
)

func setUpMocksWithSpies() (*mock.RemoteMetaMock, *mock.CommitFactoryMock) {
	spyAddCommit := test_util.NewSpyArguments()
	spyCreateBranch := test_util.NewSpyArguments()
	remoteMetaMock := &mock.RemoteMetaMock{spyAddCommit, spyCreateBranch}
	commitFactoryMock := &mock.CommitFactoryMock{}
	return remoteMetaMock, commitFactoryMock
}

func setUpCommitMock() (*mock.CommitMock, test_util.SpyCounter) {
	spySaveRemotely := &test_util.SpyCounterImpl{0}
	spySetNext := &test_util.SpyCounterImpl{0}
	spySetPrev := &test_util.SpyCounterImpl{0}
	return &mock.CommitMock{"mock", spySetNext, spySetPrev, spySaveRemotely, nil}, spySaveRemotely
}

func setUpMocks() (*mock.RemoteMetaMock, *mock.CommitFactoryMock) {
	remoteMetaMock := &mock.RemoteMetaMock{nil, nil}
	commitFactoryMock := &mock.CommitFactoryMock{}
	return remoteMetaMock, commitFactoryMock
}

func setUpBranch(remoteMetaMock *mock.RemoteMetaMock, commitFactoryMock *mock.CommitFactoryMock) domain.Branch {
	return domain.NewBranchImpl(
		9,
		"testBranch",
		12345,
		nil,
		0,
		remoteMetaMock,
		commitFactoryMock)
}

func Test_NewBranchImpl(t *testing.T) {
	branchFactory := &domain.BranchImplFactory{nil, nil}
	branchIs := branchFactory.NewBranchImpl(1, "test", 1234567)
	var branchShould domain.Branch = &domain.BranchImpl{AccessRight: 1, Name: "test",
		CreationDateUnix: 1234567, LastCommit: nil, Revision: 0}

	if !reflect.DeepEqual(branchIs, branchShould) {
		t.Errorf("is and should are not equal")
	}
}

func Test_NewBranchRemoteImpl(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	branchFactory := &domain.BranchImplFactory{remoteMetaMock, commitFactoryMock}
	branchIs, err := branchFactory.NewRemoteBranchImpl("implemet sth")
	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	branchShould := domain.NewBranchImpl(4, "implemet sth", 321, nil, 1,
		remoteMetaMock, commitFactoryMock)

	if !reflect.DeepEqual(branchIs, branchShould) {
		t.Errorf("is and should are not equal")
	}
}

func Test_NewBranchRemoteWithCommitsImpl(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	branchFactory := &domain.BranchImplFactory{remoteMetaMock, commitFactoryMock}
	branchIs, err := branchFactory.NewRemoteBranchImpl("master")
	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	branchShould := domain.NewBranchImpl(5, "master", 123456,
		&mock.CommitMock{"commit 1", nil, nil, nil, nil}, 1,
		remoteMetaMock, commitFactoryMock)

	if !reflect.DeepEqual(branchIs, branchShould) {
		t.Errorf("is and should are not equal")
	}
}

func Test_Branch_AddCommit(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	branch := setUpBranch(remoteMetaMock, commitFactoryMock)
	commit := commitFactoryMock.NewCommitImpl("c1", 123, "Homer")
	branch.AddCommit(commit)
	branchShould := domain.NewBranchImpl(9, "testBranch", 12345, &mock.CommitMock{"commit 1",
		nil, nil, nil, nil}, 0,
		remoteMetaMock, commitFactoryMock)
	branchShould.(*domain.BranchImpl).LastCommit = commit

	if !reflect.DeepEqual(branch, branchShould) {
		t.Errorf("is and should are not equal")
	}
}

func Test_Branch_AddCommitTwoTimes(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	branch := setUpBranch(remoteMetaMock, commitFactoryMock)
	spySetNext := &test_util.SpyCounterImpl{0}
	spySetPrev := &test_util.SpyCounterImpl{0}
	commit := &mock.CommitMock{"c1", spySetNext, spySetPrev, nil, nil}
	// commit := commitFactoryMock.NewCommitImpl("c1", 123, "Homer")
	branch.AddCommit(commit)
	branch.AddCommit(commit)

	branchShould := domain.NewBranchImpl(9, "testBranch", 12345,
		&mock.CommitMock{"commit 1", nil, nil, nil, nil}, 0,
		remoteMetaMock, commitFactoryMock)

	branchShould.(*domain.BranchImpl).LastCommit = commit

	if spySetNext.Get() != 1 || spySetPrev.Get() != 1 {
		t.Errorf("called wrong times")

	}

	if !reflect.DeepEqual(branch, branchShould) {
		t.Errorf("is and should are not equal")
	}
}

func Test_Branch_SetActive(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	spySetActive := &test_util.SpyCounterImpl{0}

	branch := setUpBranch(remoteMetaMock, commitFactoryMock)
	commit := &mock.CommitMock{"c1", nil, nil, nil, spySetActive}

	branch.AddCommit(commit)
	//branch.SetActive()

	if spySetActive.Get() != 1 {
		t.Errorf("called wrong times")
	}
}

func Test_Branch_Merge(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocks()
	branch := setUpBranch(remoteMetaMock, commitFactoryMock)
	branchMock := mock.NewBranchMock()
	err := branch.Merge(branchMock, 123, "Bart")
	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}
	if branch.GetLastCommit().(*mock.CommitMock).Name != "THIS_IS_MERGE" {
		t.Errorf("Commit name differs")
	}
}

func Test_Branch_Get_Last_Commit(t *testing.T) {
	branch := setUpBranch(nil, nil)
	spySetNext := &test_util.SpyCounterImpl{0}
	spySetPrev := &test_util.SpyCounterImpl{0}

	commit1 := &mock.CommitMock{"c1", spySetNext, spySetPrev, nil, nil}
	commit2 := &mock.CommitMock{"c2", spySetNext, spySetPrev, nil, nil}

	// commit := commitFactoryMock.NewCommitImpl("c1", 123, "Homer")
	branch.AddCommit(commit1)
	branch.AddCommit(commit2)

	if branch.GetLastCommit().(*mock.CommitMock).Name != "c2" {
		t.Errorf("Wrong branch returned")
	}

}

func Test_Branch_SaveRemotely_RemoteBranchDoesNotExist(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocksWithSpies()
	commit, spySaveRemotely := setUpCommitMock()
	branch := domain.NewBranchImpl(
		9,
		"testBranch",
		12345,
		commit,
		0,
		remoteMetaMock,
		commitFactoryMock)

	isLocalBranchUpToDate, err := branch.SaveRemotely()
	if err != nil {
		t.Errorf(err.Error())
	}
	if isLocalBranchUpToDate == false {
		t.Errorf("invalid revision")
	}
	/*
		1: meta.GetBranches() // no validation
		2: meta.createBranch // "testBranch", 12345,  9
		3: saveRemotely // called 1 time
		4; Add Commit called with "blabla" "mock" 0

		branch.Revision == 1
	*/

	if remoteMetaMock.SpyCreateBranch.GetNumberOfCalled() != 1 ||
		remoteMetaMock.SpyCreateBranch.GetArgumentAt(0, 0).(string) != "testBranch" ||
		remoteMetaMock.SpyCreateBranch.GetArgumentAt(0, 1).(int64) != 12345 ||
		remoteMetaMock.SpyCreateBranch.GetArgumentAt(0, 2).(int64) != 9 {
		t.Errorf("CreateBranch call error")
	}

	if spySaveRemotely.Get() != 1 {
		t.Errorf("Save Remotely is not called one times")
	}

	if remoteMetaMock.SpyAddCommit.GetNumberOfCalled() != 1 ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 0).(string) != "blabla" ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 1).(string) != "testBranch" ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 2).(int64) != 0 {
		t.Errorf("AddCommit call error")
	}

}

func Test_Branch_SaveRemotely_RemoteBranchDoesExist(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocksWithSpies()
	commit, spySaveRemotely := setUpCommitMock()
	branch := domain.NewBranchImpl(
		9,
		"master",
		12345,
		commit,
		1,
		remoteMetaMock,
		commitFactoryMock)

	isLocalBranchUpToDate, err := branch.SaveRemotely()
	if err != nil {
		t.Errorf(err.Error())
	}
	if isLocalBranchUpToDate == false {
		t.Errorf("invalid revision")
	}
	/*
		1: meta.GetBranches() 	// no validation
		2: SaveRemotely()		// called 1 time
		3: AddCommit			// iamaaddress mock 0
		branch.Revision == 1
	*/

	if spySaveRemotely.Get() != 1 {
		t.Errorf("Save Remotely is not called one times")
	}

	if remoteMetaMock.SpyAddCommit.GetNumberOfCalled() != 1 ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 0).(string) != "blabla" ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 1).(string) != "master" ||
		remoteMetaMock.SpyAddCommit.GetArgumentAt(0, 2).(int64) != 1 {
		t.Errorf("AddCommit call error")
	}

}

func Test_Branch_SaveRemotely_RemoteBranchDoesExist_No_Local_Commits(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocksWithSpies()
	branch := domain.NewBranchImpl(
		9,
		"master",
		12345,
		nil,
		1,
		remoteMetaMock,
		commitFactoryMock)

	isLocalBranchUpToDate, err := branch.SaveRemotely()
	if err != nil {
		t.Errorf(err.Error())
	}
	if isLocalBranchUpToDate == true {
		t.Errorf("detects a commit but no commits should be available")
	}

}

func Test_Branch_UpdateFromRemote_BranchDoesNotExist(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocksWithSpies()
	commit, _ := setUpCommitMock()
	branch := domain.NewBranchImpl(
		9,
		"NOT_EXIST",
		12345,
		commit,
		1,
		remoteMetaMock,
		commitFactoryMock)

	doesBranchExist, err := branch.UpdateFromRemote(789, "LarsLarsen")
	if err != nil {
		t.Errorf(err.Error())
	}

	if doesBranchExist == true {
		t.Errorf("detects a branch but no branch should be available")
	}

}

func Test_Branch_UpdateFromRemote_BranchDoesExist(t *testing.T) {
	remoteMetaMock, commitFactoryMock := setUpMocksWithSpies()
	commit, _ := setUpCommitMock()
	branch := domain.NewBranchImpl(
		9,
		"master",
		12345,
		commit,
		1,
		remoteMetaMock,
		commitFactoryMock)

	doesBranchExist, err := branch.UpdateFromRemote(789, "LarsLarsen")
	if err != nil {
		t.Errorf(err.Error())
	}

	if doesBranchExist == false {
		t.Errorf("branch not found")
	}

	if branch.GetLastCommit().GetName() != "THIS_IS_MERGE" {
		t.Errorf("wrong last commit")
	}

}
