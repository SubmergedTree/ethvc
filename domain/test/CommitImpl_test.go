package test

import (
	"ethVC/domain"
	"ethVC/domain/test/mock"
	"ethVC/test_util"
	"fmt"
	"reflect"
	"testing"
)

var remDatSpy = test_util.NewStrSpy()
var remUploadDatSpy = test_util.NewStrSpy()

var uiMock = &mock.UserInterfaceMock{Spy: test_util.NewStrSpy()}
var remDatMock = &mock.RemoteDataMock{GetSpy: remDatSpy, UploadSpy: remUploadDatSpy}
var fileFactoryMock = &mock.FileFactoryMock{remDatMock, uiMock, fileIO}
var realFileFactory = &domain.FileFactoryImpl{remDatMock, uiMock, fileIO}

var commitFactory = &domain.CommitFactoryImpl{remDatMock, fileFactoryMock, fileIO}

const commitFirstJson = "{" +
	"\"snapshot\": [{" +
	"\"address\": \"aaa\"," +
	"\"path\": \"ppp\"" +
	"}]," +
	"\"author\": \"Homer\"," +
	"\"name\": \"c1\"," +
	"\"creationDateUnix\": 1234," +
	"\"previous\": \"1\"" +
	"}"

const commitMidJson = "{" +
	"\"snapshot\": [{" +
	"\"address\": \"aaa\"," +
	"\"path\": \"ppp\"" +
	"}]," +
	"\"author\": \"Bart\"," +
	"\"name\": \"c2\"," +
	"\"creationDateUnix\": 1234," +
	"\"previous\": \"2\"" +
	"}"

const commitLastJson = "{" +
	"\"snapshot\": [{" +
	"\"address\": \"aaa\"," +
	"\"path\": \"ppp\"" +
	"}]," +
	"\"author\": \"Lisa\"," +
	"\"name\": \"c3\"," +
	"\"creationDateUnix\": 1234," +
	"\"previous\": \"\"" +
	"}"

const commitSingle = "{" +
	"\"snapshot\": [{" +
	"\"address\": \"aaa\"," +
	"\"path\": \"ppp\"" +
	"}]," +
	"\"author\": \"Homer\"," +
	"\"name\": \"c1\"," +
	"\"creationDateUnix\": 1234," +
	"\"previous\": \"\"" +
	"}"

func Test_NewCommitImpl(t *testing.T) {
	defer func() { commitFactory.RemoteData = remDatMock }()
	commitFactory.RemoteData = nil
	commitIs := commitFactory.NewCommitImpl("test", 12345, "Moe")

	var commitShould domain.Commit = &domain.CommitImpl{
		Name:             "test",
		Files:            commitIs.(*domain.CommitImpl).Files,
		CreationDateUnix: 12345,
		Previous:         nil,
		Author:           "Moe"}
	//fmt.Println(commitIs)
	//fmt.Println(commitShould)

	//	commitShould := commitFactory.NewCommitImpl("test", 12345, "Moe")
	//	commitShould.(*domain.CommitImpl).Files = commitIs.(*domain.CommitImpl).Files

	if !reflect.DeepEqual(commitIs, commitShould) {
		t.Errorf("is and should are not equal")
	}

}

func Test_NewRemoteCommitImpl(t *testing.T) {
	remDatSpy.CallNthReturnStr(commitFirstJson)
	remDatSpy.CallNthReturnStr(commitMidJson)
	remDatSpy.CallNthReturnStr(commitLastJson)

	commitFactory.RemoteData = remDatMock

	//	commitIs, err := commitFactory.NewRemoteCommitImpl("123abc", fileFactoryMock, nil, uiMock)
	commitIs, err := commitFactory.NewRemoteCommitImpl("123abc")

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	commitShouldFirst := commitFactory.NewCommitImpl("c1", 1234, "Homer")
	commitShouldMid := commitFactory.NewCommitImpl("c2", 1234, "Bart")
	commitShouldLast := commitFactory.NewCommitImpl("c3", 1234, "Lisa")

	commitShouldFirst.(*domain.CommitImpl).Files = commitIs.(*domain.CommitImpl).Files
	commitShouldMid.(*domain.CommitImpl).Files = commitIs.(*domain.CommitImpl).Files
	commitShouldLast.(*domain.CommitImpl).Files = commitIs.(*domain.CommitImpl).Files

	commitShouldFirst.SetPrevious(commitShouldMid)
	commitShouldMid.SetNext(commitShouldFirst)
	commitShouldMid.SetPrevious(commitShouldLast)
	commitShouldLast.SetNext(commitShouldMid)
	commitShouldFirst.GetPrevious().SetNext(nil)

	if !reflect.DeepEqual(commitIs, commitShouldFirst) {
		t.Errorf("is and should are not equal")
		fmt.Println(commitIs)
		fmt.Println("----------------")
		fmt.Println(commitShouldFirst)
	}
	remDatSpy.Reset()
}

func Test_NewRemoteCommitImpl1Commit(t *testing.T) {
	remDatSpy.CallNthReturnStr(commitSingle)
	commitFactory.RemoteData = remDatMock

	commitIs, err := commitFactory.NewRemoteCommitImpl("123abc")
	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	commitShouldFirst := commitFactory.NewCommitImpl("c1", 1234, "Homer")
	commitShouldFirst.(*domain.CommitImpl).Files = commitIs.(*domain.CommitImpl).Files

	if !reflect.DeepEqual(commitIs, commitShouldFirst) {
		t.Errorf("is and should are not equal")
		fmt.Println(commitIs)
		fmt.Println("----------------")
		fmt.Println(commitShouldFirst)
	}
	remDatSpy.Reset()
}

func Test_NewMergeCommit(t *testing.T) {
	uiMock.Spy.Reset()
	uiMock.Spy.CallNthReturnStr("1")
	uiMock.Spy.CallNthReturnStr("2")
	uiMock.Spy.CallNthReturnStr("2")

	commit1 := commitFactory.NewCommitImpl("c1", 1234, "Homer")
	commit1.AddFile(realFileFactory.NewFromData("abc \n foo \n uu", "/bla/foo.txt"))
	commit1.AddFile(realFileFactory.NewFromData("123 \n test", "/foo.txt"))

	commit2 := commitFactory.NewCommitImpl("c2", 1234, "Bart")
	commit2.AddFile(realFileFactory.NewFromData("bbb \n foo \n ll", "/bla/foo.txt"))
	commit2.AddFile(realFileFactory.NewFromData("123 \n test", "/foo.txt"))
	commit2.AddFile(realFileFactory.NewFromData("blablabla", "/bazz/foo.txt"))

	mergeCommit, err := commitFactory.NewMergeCommit(666, "Marge", commit1, commit2)
	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	mergeCommitImpl := mergeCommit.(*domain.CommitImpl)
	files := mergeCommitImpl.Files

	if mergeCommitImpl.CreationDateUnix != uint(666) ||
		mergeCommitImpl.Name != "Merge Commit" ||
		mergeCommitImpl.Author != "Marge" {
		t.Errorf("commit data does not match")
	}

	if !isStringArrayEqual(files[0].GetContentLineByLine(), []string{"abc ", " foo ", " ll"}) || files[0].GetPath() != "/bla/foo.txt" ||
		!isStringArrayEqual(files[1].GetContentLineByLine(), []string{"123 ", " test"}) || files[1].GetPath() != "/foo.txt" ||
		!isStringArrayEqual(files[2].GetContentLineByLine(), []string{"blablabla"}) || files[2].GetPath() != "/bazz/foo.txt" {
		t.Errorf("file data does not match")
	}

}

func isStringArrayEqual(f, s []string) bool {
	if len(f) != len(s) {
		return false
	}
	for idx, e := range f {
		if e != s[idx] {
			return false
		}
	}
	return true
}

//var testFirst = &domain.CommitImpl{"first", nil,
//	12345, nil, nil, "Moe", remDatMock}

var testFirst *domain.CommitImpl

//var testLast = &domain.CommitImpl{"last", nil,
//	12345, nil, nil, "Moe", remDatMock}
var testLast *domain.CommitImpl

var tf, _ = fileFactoryMock.NewFromPath("bludaobd")

//var testCommit = &domain.CommitImpl{"test", []domain.File{tf},
//	12345, testFirst, testLast, "Moe", remDatMock}
var testCommit *domain.CommitImpl

func setupTestCommits() {
	testFirst = commitFactory.NewCommitImpl("first", 12345, "Moe").(*domain.CommitImpl)
	testLast = commitFactory.NewCommitImpl("last", 12345, "Moe").(*domain.CommitImpl)
	testCommit = commitFactory.NewCommitImpl("test", 12345, "Moe").(*domain.CommitImpl)

	testCommit.Previous = testFirst
	testCommit.SetNext(testLast)

	testCommit.Files = []domain.File{tf}
}

func Test_AddFile(t *testing.T) {
	setupTestCommits()
	file, _ := fileFactoryMock.NewFromPath("blabl")
	testCommit.AddFile(file)
	if !reflect.DeepEqual(file, testCommit.Files[1]) || len(testCommit.Files) != 2 ||
		!reflect.DeepEqual(tf, testCommit.Files[0]) {
		t.Errorf("files are not matching")
	}
}

func Test_GetPrevious(t *testing.T) {
	setupTestCommits()
	p := testCommit.GetPrevious()

	if !reflect.DeepEqual(p, testFirst) {
		t.Errorf("commits are not equal")
	}
}

func Test_GetNext(t *testing.T) {
	setupTestCommits()
	p := testCommit.GetNext()

	if !reflect.DeepEqual(p, testLast) {
		t.Errorf("commits are not equal")
	}
}

/*
func Test_SetNext(t *testing.T) {
	setupTestCommits()
	testCommit.SetNext(nil)

	if testCommit.next != nil {
		t.Errorf("next not set")
	}
	testCommit.SetNext(testLast)
}*/

func Test_SetPrev(t *testing.T) {
	setupTestCommits()
	testCommit.SetPrevious(nil)

	if testCommit.Previous != nil {
		t.Errorf("next not set")
	}
	testCommit.Previous = testFirst
}

func Test_SetActive(t *testing.T) {
	setupTestCommits()
	spyCounter := &test_util.SpyCounterImpl{0}
	testCommit.Files = []domain.File{&mock.FileImplMock{spyCounter}, &mock.FileImplMock{spyCounter}}
	testCommit.SetActive()
	if spyCounter.Get() != 2 {
		t.Errorf("called Write wrong times")
	}

}

// TODO fix
/*
func Test_SaveRemotely(t *testing.T) {
	commitFactory.RemoteData = remDatMock

	remDatSpy.CallNthReturnStr(commitFirstJson)
	remDatSpy.CallNthReturnStr(commitMidJson)
	remDatSpy.CallNthReturnStr(commitLastJson)

	remUploadDatSpy.CallNthReturnStr("c3addressFile")
	remUploadDatSpy.CallNthReturnStr("c3addressRemote")

	remUploadDatSpy.CallNthReturnStr("c2addressFile")
	remUploadDatSpy.CallNthReturnStr("c2addressRemote")

	remUploadDatSpy.CallNthReturnStr("c1addressFile")
	remUploadDatSpy.CallNthReturnStr("c1addressRemote")

	commitIs, err := commitFactory.NewRemoteCommitImpl("123abc")

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	lastaddress, err := commitIs.SaveRemotely()

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	if lastaddress != "c1addressRemote" {
		t.Errorf("address of last remotely stored commit is wrong")
	}

	if remUploadDatSpy.GetNumberOfCalled() != 6 {
		t.Errorf("upload data was called wrong times")
	}

	uploadStringCalledWith := remUploadDatSpy.GetCalledWith()

	if uploadStringCalledWith[0] != "foobar" {
		t.Errorf("uploaded data is wrong")
	}

	if uploadStringCalledWith[1] != "{\"snapshot\":[{\"address\":\"c3addressFile\",\"path\":\"ppp\"}],\"author\":\"Lisa\",\"name\":\"c3\",\"creationDateUnix\":1234,\"previous\":\"\"}" {
		t.Errorf("uploaded data is wrong")
	}

	if uploadStringCalledWith[2] != "foobar" {
		t.Errorf("uploaded data is wrong")
	}

	if uploadStringCalledWith[3] != "{\"snapshot\":[{\"address\":\"c2addressFile\",\"path\":\"ppp\"}],\"author\":\"Bart\",\"name\":\"c2\",\"creationDateUnix\":1234,\"previous\":\"c3addressRemote\"}" {
		t.Errorf("uploaded data is wrong")
	}

/*	if uploadStringCalledWith[4] != "foobar" {
		t.Errorf("uploaded data is wrong")
	}

	if uploadStringCalledWith[5] != "{\"snapshot\":[{\"address\":\"c1addressFile\",\"path\":\"ppp\"}],\"author\":\"Homer\",\"name\":\"c1\",\"creationDateUnix\":1234,\"previous\":\"c2addressRemote\"}" {
		t.Errorf("uploaded data is wrong")
	}*/
//}
