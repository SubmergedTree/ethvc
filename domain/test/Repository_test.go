package test

import (
	"ethVC/domain"
	"ethVC/domain/test/mock"
	"reflect"
	"testing"
)

func setUp() *domain.RepositoryFactoryImpl {
	persistMock := mock.PersistMock{}
	remoteMeta := mock.RemoteMetaMock{nil, nil}
	branchMockFactory := &mock.BranchMockFactory{}
	return &domain.RepositoryFactoryImpl{&remoteMeta, persistMock, branchMockFactory}
}

func Test_NewRepository(t *testing.T) {
	repoFactory := setUp()
	repoFactory.RemoteMeta = nil
	repoFactory.Persist = nil
	repoFactory.BranchFactory = nil
	repoIs := repoFactory.NewRepository("Finntroll")
	bla := repoIs.(*domain.RepositoryImpl)
	var branches []domain.Branch
	repoShould := &domain.RepositoryImpl{Branches: branches, ActiveBranch: "", AuthorName: "Finntroll"}

	if !reflect.DeepEqual(bla, repoShould) {
		t.Errorf("repo objects is wrong")
	}
}

/*
func Test_NewRepositoryFromFile(t *testing.T) {
	repoFactory := setUp()
	repoIs, err := repoFactory.NewRepositoryFromFile()

	if err != nil {
		t.Errorf(err.Error())
	}
	repoShould := repoFactory.NewRepository("Trollfest")


	if !reflect.DeepEqual(repoIs, repoShould) {
		t.Errorf("repo objects is wrong")
	}
}*/

func Test_GetRemoteBranchNames(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Eluveitie")
	branchNames, err := repo.GetRemoteBranchNames()
	if err != nil {
		t.Errorf(err.Error())
	}

	if branchNames[0] != "implemet sth" || branchNames[1] != "master" {
		t.Error("Wrong branch names returned")
	}
}

func Test_GetLocalBranchName(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Turisas").(*domain.RepositoryImpl)
	var branches []domain.Branch
	branches = append(branches, mock.NewBranchMock())
	repo.Branches = branches
	branchNames := repo.GetLocalBranchNames()

	if branchNames[0] != "Korpiklaani" || len(branchNames) != 1 {
		t.Error("Wrong branch names returned")
	}
}

func Test_ChangeBranch(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Turisas").(*domain.RepositoryImpl)
	var branches []domain.Branch
	mockBranch := mock.NewBranchMock()
	branches = append(branches, mockBranch)
	repo.Branches = branches
	repo.ChangeBranch("Korpiklaani")

	if mockBranch.SetActiveSpyCounter.Get() != 1 {
		t.Errorf("Set active called wrong times")
	}

	if repo.ActiveBranch != "Korpiklaani" {
		t.Errorf("active branch is not equal to mockBranch")
	}
}

func Test_AddBranch_BranchAlreadyExists(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Suidakra").(*domain.RepositoryImpl)
	var branches []domain.Branch
	mockBranch := mock.NewBranchMock()
	branches = append(branches, mockBranch)
	repo.Branches = branches

	newBranchCreated, _ := repo.AddBranch("Korpiklaani", 2)

	if newBranchCreated == true {
		t.Errorf("Created new branch but a branch with the same name already exists")
	}

}

func Test_AddBranch(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Suidakra").(*domain.RepositoryImpl)
	var branches []domain.Branch
	mockBranch := mock.NewBranchMock()
	branches = append(branches, mockBranch)
	repo.Branches = branches

	newBranchCreated, _ := repo.AddBranch("Equilibrium", 2)

	if newBranchCreated == false {
		t.Errorf("Detected branch which does not exits")
	}

	if repo.ActiveBranch != "Equilibrium" {
		t.Errorf("Wrong branch is active")
	}

}

func Test_MergeBranchIntoCurrentBranch(t *testing.T) {
	repoFactory := setUp()
	repo := repoFactory.NewRepository("Suidakra").(*domain.RepositoryImpl)
	var branches []domain.Branch
	mockBranch := mock.NewBranchMock()
	mockBranch2 := mock.NewBranchMock()
	mockBranch2.Name = "Ensiferum"
	branches = append(branches, mockBranch)
	branches = append(branches, mockBranch2)

	repo.Branches = branches
	repo.ChangeBranch("Korpiklaani")
	repo.MergeBranchIntoCurrentBranch("Ensiferum")

	/*if repo.ActiveBranch.GetLastCommit().GetName() == ""{
		t.Errorf("Last commit is not a merge commit")
	}*/
}

func Test_Pull(t *testing.T) {

}

func Test_Push(t *testing.T) {

}

func Test_SaveToFile(t *testing.T) {

}
