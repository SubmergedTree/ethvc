package test

import (
	"ethVC/domain"
	"ethVC/domain/test/mock"
	"ethVC/test_util"
	"testing"
)

var uiSpy = test_util.NewStrSpy()
var remoteDataSpy = test_util.NewStrSpy()

var fileIO = mock.NewFileIOMock()
var ui = &mock.UserInterfaceMock{Spy: uiSpy}
var rd = &mock.RemoteDataMock{GetSpy: remoteDataSpy, UploadSpy: nil}

var ff = &domain.FileFactoryImpl{rd, ui, fileIO}

func Test_NewFromPath(t *testing.T) {
	file, err := ff.NewFromPath("")

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	original, ok := file.(*domain.FileImpl)
	if ok {
		if original.Content != mock.MockFileIOContent {
			t.Errorf("File content is wrong")
		}
	}
}

func Test_NewFromData(t *testing.T) {
	file := ff.NewFromData(mock.MockFileIOContent, "/bla/bla")

	original, ok := file.(*domain.FileImpl)
	if ok {
		if original.Content != mock.MockFileIOContent {
			t.Errorf("File content is wrong")
		}
	}
}

func Test_NewFromRemote(t *testing.T) {
	remoteDataSpy.ReturnEveryTimeStr(mock.RemoteDataContentMock)

	file, err := ff.NewFromRemote("783gq8d8wqcvdvasd7ca", "/bla/bla")

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	original, ok := file.(*domain.FileImpl)
	if ok {
		if original.Content != mock.RemoteDataContentMock {
			t.Errorf("File content is wrong")
		}
	}
}

var testFile1 = ff.NewFromData(mock.RemoteDataContentMock, "/bla/bla")
var testFile2 = ff.NewFromData(mock.MockFileIOContent, "/bla/bla")

func Test_GetContentLineByLine(t *testing.T) {
	lbl := testFile1.GetContentLineByLine()

	if lbl[0] != "Toinen siipi maata viisti," ||
		lbl[1] != "Toinen taivon kantta raapi," ||
		lbl[2] != "Nokka koukki kuusiloit, " {
		t.Errorf("Line By line content does not match")
	}
}

func Test_MergeWith(t *testing.T) {
	uiSpy.CallNthReturnStr("1")
	uiSpy.CallNthReturnStr("2")
	uiSpy.CallNthReturnStr("1")

	merged, err := testFile1.MergeWith(testFile2)

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}
	lbl := merged.GetContentLineByLine()
	if lbl[0] != "Toinen siipi maata viisti," ||
		lbl[1] != " unless B is a friend of A then it can play with A's privates as well" ||
		lbl[2] != "Nokka koukki kuusiloit, " {
		t.Errorf("Line By line content does not match")
	}
}

func Test_Write(t *testing.T) {
	remoteDataSpy.ReturnEveryTimeStr(mock.RemoteDataContentMock)
	testFile1.Write()

	if fileIO.Called != 1 || fileIO.NestedWriteContent != mock.RemoteDataContentMock ||
		fileIO.NestedWritePath != "/bla/bla" {
		t.Errorf("Argument mismatch")
	}
}

func Test_GetPath(t *testing.T) {
	if testFile1.GetPath() != "/bla/bla" {
		t.Errorf("Path is wrong")
	}
}

func Test_StoreRemotely(t *testing.T) {
	remoteDataSpy.ReturnEveryTimeStr(mock.RemoteDataContentMock)
	r, err := testFile1.StoreRemotely()

	if err != nil {
		t.Errorf("Error!. %s", err.Error())
	}

	if r != mock.RemoteDataContentMock {
		t.Errorf("Wrong content")
	}
}
