package mock

import "ethVC/test_util"

const RemoteDataContentMock = "Toinen siipi maata viisti,\n" +
	"Toinen taivon kantta raapi,\n" +
	"Nokka koukki kuusiloit, "

type RemoteDataMock struct {
	GetSpy    test_util.StrSpy
	UploadSpy test_util.StrSpy
}

func (RemoteDataMock) UploadFile(path string) (string, error) {
	return "a4a47f35737501272770a661beafe079577eea411c523829f0c01280c8c48c29", nil

}
func (r *RemoteDataMock) GetFile(hash string) (string, error) {
	return r.GetSpy.Return(), nil
}
func (RemoteDataMock) GetFileAsBytes(hash string) ([]byte, error) {
	return []byte(RemoteDataContentMock), nil
}

func (r *RemoteDataMock) UploadString(content string) (string, error) {
	if r.UploadSpy == nil {
		return content, nil
	}
	r.UploadSpy.CalledWith(content)
	return r.UploadSpy.Return(), nil
}
