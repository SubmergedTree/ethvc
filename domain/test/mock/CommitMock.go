package mock

import (
	"ethVC/domain"
	"ethVC/test_util"
)

type CommitMock struct {
	Name string

	SpySetNext      test_util.SpyCounter
	SpySetPrev      test_util.SpyCounter
	SpySaveRemotely test_util.SpyCounter
	SpySetActive    test_util.SpyCounter
}

type CommitFactoryMock struct {
}

func (CommitFactoryMock) NewCommitFromAttributes(attributes map[string]interface{}) (domain.Commit, error) {
	return &CommitMock{"commit 1", nil, nil, nil, nil}, nil
}

func (CommitFactoryMock) NewCommitFromPath(name string, creationDateUnix uint, author string, path string) (domain.Commit, error) {
	return &CommitMock{"commit 1", nil, nil, nil, nil}, nil
}

func (CommitFactoryMock) NewCommitImpl(name string, creationDateUnix uint, author string) domain.Commit {
	return &CommitMock{name, nil, nil, nil, nil}
}

func (CommitFactoryMock) NewRemoteCommitImpl(address string) (domain.Commit, error) {
	if address == "iamaaddress" {
		return &CommitMock{"commit 1", nil, nil, nil, nil}, nil
	}

	return &CommitMock{}, nil
}

func (CommitFactoryMock) NewMergeCommit(creationDateUnix uint, author string, commit1, commit2 domain.Commit) (domain.Commit, error) {
	return &CommitMock{"THIS_IS_MERGE", nil, nil, nil, nil}, nil
}

func (c CommitMock) GetName() string {
	return c.Name
}

func (c CommitMock) GetCreationDate() uint {
	return 1234
}

func (CommitMock) AddFile(file domain.File) {

}

func (CommitMock) GetPrevious() domain.Commit {
	return nil
}

func (c *CommitMock) SetPrevious(domain.Commit) {
	if c.SpySetPrev != nil {
		c.SpySetPrev.Increment()
	}
}

func (CommitMock) GetNext() domain.Commit {
	return nil
}

func (c *CommitMock) SetNext(nextCommit domain.Commit) {
	if c.SpySetNext != nil {
		c.SpySetNext.Increment()
	}
}

func (c *CommitMock) SetActive() {
	if c.SpySetActive != nil {
		c.SpySetActive.Increment()
	}
}

func (c *CommitMock) SaveRemotely() (string, error) {
	c.SpySaveRemotely.Increment()
	return "blabla", nil
}

func (CommitMock) GetFiles() []domain.File {
	var files []domain.File
	return files
}
