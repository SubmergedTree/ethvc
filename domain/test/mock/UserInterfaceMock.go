package mock

import (
	"ethVC/boilerplate"
	"ethVC/test_util"
)

type UserInterfaceMock struct {
	Spy test_util.StrSpy
}

func (u UserInterfaceMock) Run() boilerplate.UsecaseTO {
	return nil
}

func (UserInterfaceMock) Show(message string) {

}
func (UserInterfaceMock) ShowGeneric(a ...interface{}) {

}

func (u *UserInterfaceMock) Ask(question string) string {
	return u.Spy.Return()
}
