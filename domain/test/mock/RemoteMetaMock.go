package mock

import (
	"ethVC/boilerplate"
	"ethVC/test_util"
)

type RemoteMetaMock struct {
	SpyAddCommit    test_util.SpyArguments
	SpyCreateBranch test_util.SpyArguments
}

func (RemoteMetaMock) AddUser(newUserPublicKey string, newUserEmail string, newUserRightLevel int64) error {
	return nil
}

func (RemoteMetaMock) DeleteUser(userPublicKey string) error {
	return nil
}

func (RemoteMetaMock) UpdateUser(newUserEmail string, userPublicKey string, rightLevel int64) error {
	return nil
}

func (RemoteMetaMock) GetUsers() ([]*boilerplate.UserMetaPOD, error) {
	var user []*boilerplate.UserMetaPOD
	return user, nil
}

func (RemoteMetaMock) GetBranches() ([]*boilerplate.BranchMetaPOD, error) {
	var meta []*boilerplate.BranchMetaPOD

	meta = append(meta, &boilerplate.BranchMetaPOD{"implemet sth", 4, 321,
		"", 1})
	meta = append(meta, &boilerplate.BranchMetaPOD{"master", 5, 123456,
		"iamaaddress", 1})

	return meta, nil
}

func (r *RemoteMetaMock) CreateBranch(branchName string, unixTimeStamp int64, accessRight int64) error {
	r.SpyCreateBranch.AddArguments(branchName, unixTimeStamp, accessRight)
	return nil
}

func (r *RemoteMetaMock) AddCommit(remoteFileAddress string, branchName string, revision int64) error {
	r.SpyAddCommit.AddArguments(remoteFileAddress, branchName, revision)
	return nil
}
