package mock

const MockFileIOContent = "Only A can play with it's privates, B cannot,\n" +
	" unless B is a friend of A then it can play with A's privates as well"

type FileIOMock struct {
	NestedWritePath    string
	NestedWriteContent string
	Called             uint
}

func NewFileIOMock() *FileIOMock {
	return &FileIOMock{"", "", 0}
}

func (FileIOMock) GetAllFilePaths(top string) ([]string, error) {
	return []string{"/foo/Homer.rm", "/Bart.c"}, nil
}

func (FileIOMock) ReadFile(path string) (string, error) {
	return MockFileIOContent, nil
}

func (f *FileIOMock) NestedWrite(path string, content string) error {
	f.Called++
	f.NestedWritePath = path
	f.NestedWriteContent = content
	return nil
}

func (FileIOMock) DeleteAll() error {
	return nil
}
