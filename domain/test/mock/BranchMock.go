package mock

import (
	"ethVC/domain"
	"ethVC/test_util"
)

type BranchMockFactory struct {
}

func (BranchMockFactory) NewBranchFromAttributes(interface{}) (domain.Branch, error) {
	return NewBranchMock(), nil
}

func (BranchMockFactory) NewBranchImpl(accessRight uint, name string, creationDateUnix uint) domain.Branch {
	return NewBranchMock()
}

func (BranchMockFactory) NewRemoteBranchImpl(branchName string) (domain.Branch, error) {
	return NewBranchMock(), nil
}

func (BranchMockFactory) NewBranchWithLastCommit(accessRight uint, name string, creationDateUnix uint, lastCommit domain.Commit) domain.Branch {
	return NewBranchMockWithName(name)
}

type BranchMock struct {
	SetActiveSpyCounter test_util.SpyCounter
	MergeSpy            test_util.SpyArguments
	Name                string
}

func (b BranchMock) CreateCommit(name string, creationDateUnix uint, author string, topPath string) error {
	return nil
}

func NewBranchMock() *BranchMock {
	spyCntSetActive := &test_util.SpyCounterImpl{0}
	mergeSpy := test_util.NewSpyArguments()
	return &BranchMock{spyCntSetActive, mergeSpy, "Korpiklaani"}
}

func NewBranchMockWithName(name string) *BranchMock {
	spyCntSetActive := &test_util.SpyCounterImpl{0}
	mergeSpy := test_util.NewSpyArguments()
	return &BranchMock{spyCntSetActive, mergeSpy, name}
}

func (BranchMock) AddCommit(c domain.Commit) {

}
func (b *BranchMock) SetActive() {
	if b.SetActiveSpyCounter != nil {
		b.SetActiveSpyCounter.Increment()
	}
}

func (BranchMock) SaveRemotely() (bool, error) {
	return true, nil
}

func (BranchMock) UpdateFromRemote(creationDateUnix uint, author string) (bool, error) {
	return true, nil
}

func (b *BranchMock) Merge(mergeInto domain.Branch, creationDateUnix uint, author string) error {
	if b.MergeSpy != nil {
		b.MergeSpy.AddArguments(mergeInto, creationDateUnix, author)
	}
	return nil
}

func (BranchMock) GetLastCommit() domain.Commit {
	return nil
}

func (b *BranchMock) GetName() string {
	return b.Name
}
