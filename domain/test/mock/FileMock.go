package mock

import (
	"ethVC/boilerplate"
	"ethVC/domain"
	"ethVC/test_util"
)

type FileFactoryMock struct {
	RemoteData    boilerplate.RemoteData
	UserInterface boilerplate.UserInterface
	FileIO        boilerplate.FileIO
}

func (f *FileFactoryMock) NewFromPath(path string) (domain.File, error) {
	return domain.NewFileImpl("foobar", path, f.FileIO, f.UserInterface, f.RemoteData), nil
}

func (f *FileFactoryMock) NewFromData(content string, path string) domain.File {
	return domain.NewFileImpl("foobar", path, f.FileIO, f.UserInterface, f.RemoteData)

}

func (f *FileFactoryMock) NewFromRemote(address string, path string) (domain.File, error) {
	return domain.NewFileImpl("foobar", path, f.FileIO, f.UserInterface, f.RemoteData), nil

}

type FileImplMock struct {
	Counter test_util.SpyCounter
}

func (FileImplMock) GetContentLineByLine() []string {
	panic("not implemented")
}

func (FileImplMock) MergeWith(domain.File) (domain.File, error) {
	panic("not implemented")
}

func (f *FileImplMock) Write() error {
	f.Counter.Increment()
	return nil
}

func (FileImplMock) GetPath() string {
	panic("not implemented")

}

func (FileImplMock) StoreRemotely() (string, error) {
	return "address", nil
}
