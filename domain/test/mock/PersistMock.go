package mock

import "ethVC/domain"

type PersistMock struct {
}

func (PersistMock) Serialize(obj interface{}) error {
	return nil
}

func (PersistMock) Deserialize() (interface{}, error) {
	var branches []domain.Branch
	obj := &domain.RepositoryImpl{Branches: branches, ActiveBranch: "", AuthorName: "Trollfest"}
	return obj, nil
}
