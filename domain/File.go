package domain

type FileFactory interface {
	NewFromPath(path string) (File, error)
	NewFromData(content string, path string) File
	NewFromRemote(address string, path string) (File, error)
}

type File interface {
	GetContentLineByLine() []string
	MergeWith(File) (File, error)
	Write() error
	GetPath() string
	StoreRemotely() (string, error)
}
