package domain

type CommitFactory interface {
	NewCommitImpl(name string, creationDateUnix uint, author string) Commit
	NewRemoteCommitImpl(address string) (Commit, error)
	NewMergeCommit(creationDateUnix uint, author string, commit1, commit2 Commit) (Commit, error)
	NewCommitFromAttributes(attributes map[string]interface{}) (Commit, error)
	NewCommitFromPath(name string, creationDateUnix uint, author string, path string) (Commit, error)
}

type Commit interface {
	AddFile(File)
	GetPrevious() Commit
	SetPrevious(Commit)
	GetNext() Commit
	SetNext(Commit)
	SetActive()
	SaveRemotely() (string, error)
	GetFiles() []File
	GetName() string
	GetCreationDate() uint
}
