package domain

import (
	"ethVC/boilerplate"
	"time"
)

type RepositoryImpl struct {
	Branches []Branch
	//ActiveBranch Branch
	ActiveBranch string // HEAD
	AuthorName   string // username

	remoteMeta    boilerplate.RemoteMeta
	persist       boilerplate.Persist
	branchFactory BranchFactory
}

type RepositoryFactoryImpl struct {
	RemoteMeta boilerplate.RemoteMeta
	Persist    boilerplate.Persist

	BranchFactory BranchFactory
}

func (r *RepositoryFactoryImpl) NewRepository(author string) Repository {
	var branches []Branch
	return &RepositoryImpl{branches, "", author,
		r.RemoteMeta, r.Persist, r.BranchFactory}
}

func (r *RepositoryFactoryImpl) NewRepositoryFromFile() (Repository, error) {
	//var obj interface{}
	var obj interface{}
	obj, err := r.Persist.Deserialize()
	if err != nil {
		return nil, err
	}
	des := obj.(map[string]interface{})

	var branches []Branch
	branchesAttributes, ok := des["Branches"].([]interface{})
	if ok == false {
		var b []Branch
		branches = b
	} else {

		for _, attr := range branchesAttributes {
			b, err := r.BranchFactory.NewBranchFromAttributes(attr)
			if err != nil {
				return nil, err
			}
			branches = append(branches, b)
		}
	}
	//fmt.Println(branches)
	//fmt.Println(des["ActiveBranch"].(string))
	activeBranch := des["ActiveBranch"].(string)

	author := des["AuthorName"].(string)
	repo := &RepositoryImpl{branches, activeBranch, author,
		r.RemoteMeta, r.Persist, r.BranchFactory}
	return repo, nil
}

func (r *RepositoryImpl) GetRemoteBranchNames() ([]string, error) {
	branches, err := r.remoteMeta.GetBranches()
	var names []string

	if err != nil {
		return names, err
	}

	for _, branch := range branches {
		names = append(names, branch.Name)
	}
	return names, nil
}

func (r *RepositoryImpl) GetLocalBranchNames() []string {
	var names []string
	for _, branch := range r.Branches {
		names = append(names, branch.GetName())
	}
	return names

}

func (r *RepositoryImpl) ChangeBranch(name string) {
	for _, branch := range r.Branches {
		if branch.GetName() == name {
			branch.SetActive()
			//	r.ActiveBranch = branch
			r.ActiveBranch = branch.GetName()
			return
		}
	}
	// TODO return that no branch was found
}

func (r *RepositoryImpl) MergeBranchIntoCurrentBranch(otherBranchName string) (bool, error) {
	//	fmt.Println(otherBranchName)
	other := r.getBranchByName(otherBranchName)
	//	fmt.Println(other)
	if other == nil {
		return false, nil
	}

	if r.ActiveBranch == "" {
		r.ActiveBranch = other.GetName()
	} else {
		err := r.getActiveBranch().Merge(other, getCurrentUnixTimeStamp(), r.AuthorName)
		if err != nil {
			return true, err
		}
	}

	return true, nil
}

func (r *RepositoryImpl) Pull() (bool, error) {
	if r.ActiveBranch != "" {
		return r.getActiveBranch().UpdateFromRemote(getCurrentUnixTimeStamp(), r.AuthorName)
	}
	return false, nil
}

func (r *RepositoryImpl) Push() (bool, error) {
	if r.ActiveBranch != "" {
		return r.getActiveBranch().SaveRemotely()
	}
	return false, nil
}

func (r *RepositoryImpl) SaveToFile() error {
	return r.persist.Serialize(r)
}

func (r *RepositoryImpl) AddBranch(branchName string, accessRight uint) (bool, error) {
	if r.getBranchByName(branchName) != nil {
		return false, nil
	}

	var lastCommit Commit
	if r.ActiveBranch != "" {
		lastCommit = r.getActiveBranch().GetLastCommit()
	}

	newBranch := r.branchFactory.NewBranchWithLastCommit(accessRight, branchName, getCurrentUnixTimeStamp(), lastCommit)
	r.Branches = append(r.Branches, newBranch)
	r.ChangeBranch(branchName)
	return true, nil
}

func (r *RepositoryImpl) Commit(commitName string) (bool, error) {
	activeBranch := r.getActiveBranch()
	if activeBranch != nil {
		err := activeBranch.CreateCommit(commitName, getCurrentUnixTimeStamp(), r.AuthorName, "./")
		if err != nil {
			return false, err
		}
		return true, nil
	}
	return false, nil
}

func getCurrentUnixTimeStamp() uint {
	return uint(time.Now().Unix())
}

func (r *RepositoryImpl) getBranchByName(name string) Branch {
	for _, b := range r.Branches {
		if b.GetName() == name {
			return b
		}
	}
	return nil
}

func (r *RepositoryImpl) getActiveBranch() Branch {
	for _, branch := range r.Branches {
		if branch.GetName() == r.ActiveBranch {
			return branch
		}
	}
	return nil
}
