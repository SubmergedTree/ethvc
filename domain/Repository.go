package domain

// TODO in documentation repositories are missing in drawings.

type Repository interface {
	GetRemoteBranchNames() ([]string, error)
	GetLocalBranchNames() []string
	ChangeBranch(name string)
	AddBranch(branchName string, accessRight uint) (bool, error)
	MergeBranchIntoCurrentBranch(otherBranchName string) (bool, error)
	Pull() (bool, error)
	Push() (bool, error)
	SaveToFile() error
	Commit(commitName string) (bool, error)
}

type RepositoryFactory interface {
	NewRepository(author string) Repository
	NewRepositoryFromFile() (Repository, error)
}
