# Start local ethreum instance

## 1. Terminal
- geth --datadir <folder which contains geth and keystore e.g. ./blkchain> -port 3000 --networkid 58343 --nodiscover --maxpeers=0  --rpc --rpcport 8543 --rpcaddr 127.0.0.1 --rpccorsdomain "*" --rpcapi "eth,net,web3,personal,miner"

## 2. Terminal
- geth attach http://127.0.0.1:8543

- miner.start()


# Setup local blockchain
https://hackernoon.com/set-up-a-private-ethereum-blockchain-and-deploy-your-first-solidity-smart-contract-on-the-caa8334c343d

