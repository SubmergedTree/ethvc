// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package ContractImpl

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// EthVCABI is the input ABI used to generate the binding from.
const EthVCABI = "[{\"constant\":true,\"inputs\":[{\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getUserRightLevelAtIndex\",\"outputs\":[{\"name\":\"rightLevel\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"newUserAddress\",\"type\":\"address\"},{\"name\":\"email\",\"type\":\"bytes32\"},{\"name\":\"rightLevel\",\"type\":\"uint256\"}],\"name\":\"addAuthorizedUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"userToDelete\",\"type\":\"address\"}],\"name\":\"deleteUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"userAddress\",\"type\":\"address\"}],\"name\":\"isUserKnown\",\"outputs\":[{\"name\":\"isKnown\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getUserEmailAtIndex\",\"outputs\":[{\"name\":\"email\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getBranchNames\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32[]\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"branchName\",\"type\":\"bytes32\"}],\"name\":\"getBranchByName\",\"outputs\":[{\"name\":\"\",\"type\":\"bytes32\"},{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"uint256\"},{\"name\":\"\",\"type\":\"bytes32\"},{\"name\":\"\",\"type\":\"bytes32\"},{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getUserCount\",\"outputs\":[{\"name\":\"count\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"branchName\",\"type\":\"bytes32\"},{\"name\":\"indexFileUpper\",\"type\":\"bytes32\"},{\"name\":\"indexFileLower\",\"type\":\"bytes32\"},{\"name\":\"revision\",\"type\":\"uint256\"}],\"name\":\"addCommit\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"userToUpdate\",\"type\":\"address\"},{\"name\":\"newEmail\",\"type\":\"bytes32\"},{\"name\":\"newRightLevel\",\"type\":\"uint256\"}],\"name\":\"updateUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"rightLevel\",\"type\":\"uint256\"},{\"name\":\"name\",\"type\":\"bytes32\"},{\"name\":\"creationDateUnix\",\"type\":\"uint256\"}],\"name\":\"addBranch\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"getUserAtIndex\",\"outputs\":[{\"name\":\"userAddress\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"name\":\"email\",\"type\":\"bytes32\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"userAdress\",\"type\":\"address\"},{\"indexed\":false,\"name\":\"successCode\",\"type\":\"uint256\"}],\"name\":\"UserEvent\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"name\":\"branchName\",\"type\":\"bytes32\"},{\"indexed\":false,\"name\":\"successCode\",\"type\":\"uint256\"}],\"name\":\"VCEvent\",\"type\":\"event\"}]"

// EthVCBin is the compiled bytecode used for deploying new contracts.
const EthVCBin = `0x6080604052600080556001805560028055600380556000600455600160055560026006556003600755600460085560056009556006600a556007600b556008600c556009600d55600a600e55600b600f55600c601055600d601155600e60125534801561006b57600080fd5b50604051602080610c10833981016040525160035461009690339083906401000000006100da810204565b600a5460408051338152602081019290925280517f1fb035d1f559adcc493bdcf3a474e65352c4bb4d8d6ab9ad62fab2386bb439ea9281900390910190a150610145565b6013805460018181019092557f66de8ffda797e3de9c05e8fc57b3bf0ec28a930d40b0d285d93c06501cf6a09081018054600160a060020a03909616600160a060020a0319909616861790556000948552601460205260409094206002810194909455918355910155565b610abc806101546000396000f3006080604052600436106100b95763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166301456d2181146100be578063556cb493146100e85780635c60f226146101115780636224f36f146101325780637583e1aa14610167578063aa7ee23b1461017f578063ade98bf9146101e4578063b5cb15f71461022f578063c066ea0a14610244578063cc32588a14610265578063f8f8e53c1461028c578063ffcc7bbf146102aa575b600080fd5b3480156100ca57600080fd5b506100d66004356102de565b60408051918252519081900360200190f35b3480156100f457600080fd5b5061010f600160a060020a036004351660243560443561031d565b005b34801561011d57600080fd5b5061010f600160a060020a03600435166103d2565b34801561013e57600080fd5b50610153600160a060020a0360043516610545565b604080519115158252519081900360200190f35b34801561017357600080fd5b506100d66004356105a2565b34801561018b57600080fd5b506101946105de565b60408051602080825283518183015283519192839290830191858101910280838360005b838110156101d05781810151838201526020016101b8565b505050509050019250505060405180910390f35b3480156101f057600080fd5b506101fc600435610638565b604080519687526020870195909552858501939093526060850191909152608084015260a0830152519081900360c00190f35b34801561023b57600080fd5b506100d661066c565b34801561025057600080fd5b5061010f600435602435604435606435610672565b34801561027157600080fd5b5061010f600160a060020a03600435166024356044356107b6565b34801561029857600080fd5b5061010f600435602435604435610829565b3480156102b657600080fd5b506102c2600435610951565b60408051600160a060020a039092168252519081900360200190f35b6000806013838154811015156102f057fe5b600091825260208083209190910154600160a060020a031682526014905260409020600101549392505050565b61032633610545565b801561033b575060036103383361097d565b10155b801561034d575061034b83610545565b155b156103975761035d83838361099b565b60055460408051600160a060020a038616815260208101929092528051600080516020610a518339815191529281900390910190a16103cd565b60045460408051600160a060020a038616815260208101929092528051600080516020610a518339815191529281900390910190a15b505050565b6000806103de33610545565b80156103f3575060036103f03361097d565b10155b8015610403575061040383610545565b1561050b57600160a060020a03831660009081526014602052604081206002810180548383556001909201839055919091556013805491935090600019810190811061044b57fe5b60009182526020909120015460138054600160a060020a03909216925082918490811061047457fe5b6000918252602080832091909101805473ffffffffffffffffffffffffffffffffffffffff1916600160a060020a03948516179055918316815260149091526040902060020182905560138054906104d0906000198301610a13565b5060095460408051600160a060020a038616815260208101929092528051600080516020610a518339815191529281900390910190a16103cd565b60085460408051600160a060020a038616815260208101929092528051600080516020610a518339815191529281900390910190a1505050565b60135460009015156105595750600061059d565b600160a060020a03821660008181526014602052604090206002015460138054909190811061058457fe5b600091825260209091200154600160a060020a03161490505b919050565b6000806013838154811015156105b457fe5b600091825260208083209190910154600160a060020a031682526014905260409020549392505050565b6060601580548060200260200160405190810160405280929190818152602001828054801561062d57602002820191906000526020600020905b81548152600190910190602001808311610618575b505050505090505b90565b6000908152601660205260409020600181015460028201548254600584015460068501546003909501549395929491939092565b60135490565b60008481526016602052604090206003015415156106b7576010546040805186815260208101929092528051600080516020610a718339815191529281900390910190a15b6000848152601660205260409020600301548110156106fd576012546040805186815260208101929092528051600080516020610a718339815191529281900390910190a15b61070633610545565b801561072857506000848152601660205260409020546107253361097d565b10155b1561078357600084815260166020908152604091829020600581018690556006810185905560030180546001019055600e548251878152918201528151600080516020610a71833981519152929181900390910190a16107b0565b6011546040805186815260208101929092528051600080516020610a718339815191529281900390910190a15b50505050565b6107bf33610545565b80156107d4575060036107d13361097d565b10155b1561050b57600160a060020a0383166000818152601460209081526040918290208581556001018490556007548251938452908301528051600080516020610a518339815191529281900390910190a16103cd565b61083233610545565b1580610846575060026108443361097d565b105b1561087857600d546040805184815260208101929092528051600080516020610a718339815191529281900390910190a15b6000828152601660205260409020600301541515610920576015805460018181019092557f55f448fdea98c4d29eb340757ef0a66cd03dbb9538908a6a81d96026b71ec4758101849055600084815260166020908152604091829020600481019390935582840186905586835560038301939093556002909101839055600b548151858152928301528051600080516020610a718339815191529281900390910190a16103cd565b600c546040805184815260208101929092528051600080516020610a718339815191529281900390910190a1505050565b600060138281548110151561096257fe5b600091825260209091200154600160a060020a031692915050565b600160a060020a031660009081526014602052604090206001015490565b6013805460018181019092557f66de8ffda797e3de9c05e8fc57b3bf0ec28a930d40b0d285d93c06501cf6a09081018054600160a060020a0390961673ffffffffffffffffffffffffffffffffffffffff19909616861790556000948552601460205260409094206002810194909455918355910155565b8154818355818111156103cd576000838152602090206103cd91810190830161063591905b80821115610a4c5760008155600101610a38565b509056001fb035d1f559adcc493bdcf3a474e65352c4bb4d8d6ab9ad62fab2386bb439ea854cd08e1ad382bca289108e982bf8d02eb4556c7e5c3f48ec720d545d6157aca165627a7a7230582029878c7bc1e5f4e37fb83141beb709f7937f451eeab26ea40195bc7a067a44950029`

// DeployEthVC deploys a new Ethereum contract, binding an instance of EthVC to it.
func DeployEthVC(auth *bind.TransactOpts, backend bind.ContractBackend, email [32]byte) (common.Address, *types.Transaction, *EthVC, error) {
	parsed, err := abi.JSON(strings.NewReader(EthVCABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(EthVCBin), backend, email)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &EthVC{EthVCCaller: EthVCCaller{contract: contract}, EthVCTransactor: EthVCTransactor{contract: contract}, EthVCFilterer: EthVCFilterer{contract: contract}}, nil
}

// EthVC is an auto generated Go binding around an Ethereum contract.
type EthVC struct {
	EthVCCaller     // Read-only binding to the contract
	EthVCTransactor // Write-only binding to the contract
	EthVCFilterer   // Log filterer for contract events
}

// EthVCCaller is an auto generated read-only Go binding around an Ethereum contract.
type EthVCCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EthVCTransactor is an auto generated write-only Go binding around an Ethereum contract.
type EthVCTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EthVCFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type EthVCFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// EthVCSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type EthVCSession struct {
	Contract     *EthVC            // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// EthVCCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type EthVCCallerSession struct {
	Contract *EthVCCaller  // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// EthVCTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type EthVCTransactorSession struct {
	Contract     *EthVCTransactor  // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// EthVCRaw is an auto generated low-level Go binding around an Ethereum contract.
type EthVCRaw struct {
	Contract *EthVC // Generic contract binding to access the raw methods on
}

// EthVCCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type EthVCCallerRaw struct {
	Contract *EthVCCaller // Generic read-only contract binding to access the raw methods on
}

// EthVCTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type EthVCTransactorRaw struct {
	Contract *EthVCTransactor // Generic write-only contract binding to access the raw methods on
}

// NewEthVC creates a new instance of EthVC, bound to a specific deployed contract.
func NewEthVC(address common.Address, backend bind.ContractBackend) (*EthVC, error) {
	contract, err := bindEthVC(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &EthVC{EthVCCaller: EthVCCaller{contract: contract}, EthVCTransactor: EthVCTransactor{contract: contract}, EthVCFilterer: EthVCFilterer{contract: contract}}, nil
}

// NewEthVCCaller creates a new read-only instance of EthVC, bound to a specific deployed contract.
func NewEthVCCaller(address common.Address, caller bind.ContractCaller) (*EthVCCaller, error) {
	contract, err := bindEthVC(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &EthVCCaller{contract: contract}, nil
}

// NewEthVCTransactor creates a new write-only instance of EthVC, bound to a specific deployed contract.
func NewEthVCTransactor(address common.Address, transactor bind.ContractTransactor) (*EthVCTransactor, error) {
	contract, err := bindEthVC(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &EthVCTransactor{contract: contract}, nil
}

// NewEthVCFilterer creates a new log filterer instance of EthVC, bound to a specific deployed contract.
func NewEthVCFilterer(address common.Address, filterer bind.ContractFilterer) (*EthVCFilterer, error) {
	contract, err := bindEthVC(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &EthVCFilterer{contract: contract}, nil
}

// bindEthVC binds a generic wrapper to an already deployed contract.
func bindEthVC(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(EthVCABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_EthVC *EthVCRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _EthVC.Contract.EthVCCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_EthVC *EthVCRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _EthVC.Contract.EthVCTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_EthVC *EthVCRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _EthVC.Contract.EthVCTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_EthVC *EthVCCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _EthVC.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_EthVC *EthVCTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _EthVC.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_EthVC *EthVCTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _EthVC.Contract.contract.Transact(opts, method, params...)
}

// GetBranchByName is a free data retrieval call binding the contract method 0xade98bf9.
//
// Solidity: function getBranchByName(branchName bytes32) constant returns(bytes32, uint256, uint256, bytes32, bytes32, uint256)
func (_EthVC *EthVCCaller) GetBranchByName(opts *bind.CallOpts, branchName [32]byte) ([32]byte, *big.Int, *big.Int, [32]byte, [32]byte, *big.Int, error) {
	var (
		ret0 = new([32]byte)
		ret1 = new(*big.Int)
		ret2 = new(*big.Int)
		ret3 = new([32]byte)
		ret4 = new([32]byte)
		ret5 = new(*big.Int)
	)
	out := &[]interface{}{
		ret0,
		ret1,
		ret2,
		ret3,
		ret4,
		ret5,
	}
	err := _EthVC.contract.Call(opts, out, "getBranchByName", branchName)
	return *ret0, *ret1, *ret2, *ret3, *ret4, *ret5, err
}

// GetBranchByName is a free data retrieval call binding the contract method 0xade98bf9.
//
// Solidity: function getBranchByName(branchName bytes32) constant returns(bytes32, uint256, uint256, bytes32, bytes32, uint256)
func (_EthVC *EthVCSession) GetBranchByName(branchName [32]byte) ([32]byte, *big.Int, *big.Int, [32]byte, [32]byte, *big.Int, error) {
	return _EthVC.Contract.GetBranchByName(&_EthVC.CallOpts, branchName)
}

// GetBranchByName is a free data retrieval call binding the contract method 0xade98bf9.
//
// Solidity: function getBranchByName(branchName bytes32) constant returns(bytes32, uint256, uint256, bytes32, bytes32, uint256)
func (_EthVC *EthVCCallerSession) GetBranchByName(branchName [32]byte) ([32]byte, *big.Int, *big.Int, [32]byte, [32]byte, *big.Int, error) {
	return _EthVC.Contract.GetBranchByName(&_EthVC.CallOpts, branchName)
}

// GetBranchNames is a free data retrieval call binding the contract method 0xaa7ee23b.
//
// Solidity: function getBranchNames() constant returns(bytes32[])
func (_EthVC *EthVCCaller) GetBranchNames(opts *bind.CallOpts) ([][32]byte, error) {
	var (
		ret0 = new([][32]byte)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "getBranchNames")
	return *ret0, err
}

// GetBranchNames is a free data retrieval call binding the contract method 0xaa7ee23b.
//
// Solidity: function getBranchNames() constant returns(bytes32[])
func (_EthVC *EthVCSession) GetBranchNames() ([][32]byte, error) {
	return _EthVC.Contract.GetBranchNames(&_EthVC.CallOpts)
}

// GetBranchNames is a free data retrieval call binding the contract method 0xaa7ee23b.
//
// Solidity: function getBranchNames() constant returns(bytes32[])
func (_EthVC *EthVCCallerSession) GetBranchNames() ([][32]byte, error) {
	return _EthVC.Contract.GetBranchNames(&_EthVC.CallOpts)
}

// GetUserAtIndex is a free data retrieval call binding the contract method 0xffcc7bbf.
//
// Solidity: function getUserAtIndex(index uint256) constant returns(userAddress address)
func (_EthVC *EthVCCaller) GetUserAtIndex(opts *bind.CallOpts, index *big.Int) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "getUserAtIndex", index)
	return *ret0, err
}

// GetUserAtIndex is a free data retrieval call binding the contract method 0xffcc7bbf.
//
// Solidity: function getUserAtIndex(index uint256) constant returns(userAddress address)
func (_EthVC *EthVCSession) GetUserAtIndex(index *big.Int) (common.Address, error) {
	return _EthVC.Contract.GetUserAtIndex(&_EthVC.CallOpts, index)
}

// GetUserAtIndex is a free data retrieval call binding the contract method 0xffcc7bbf.
//
// Solidity: function getUserAtIndex(index uint256) constant returns(userAddress address)
func (_EthVC *EthVCCallerSession) GetUserAtIndex(index *big.Int) (common.Address, error) {
	return _EthVC.Contract.GetUserAtIndex(&_EthVC.CallOpts, index)
}

// GetUserCount is a free data retrieval call binding the contract method 0xb5cb15f7.
//
// Solidity: function getUserCount() constant returns(count uint256)
func (_EthVC *EthVCCaller) GetUserCount(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "getUserCount")
	return *ret0, err
}

// GetUserCount is a free data retrieval call binding the contract method 0xb5cb15f7.
//
// Solidity: function getUserCount() constant returns(count uint256)
func (_EthVC *EthVCSession) GetUserCount() (*big.Int, error) {
	return _EthVC.Contract.GetUserCount(&_EthVC.CallOpts)
}

// GetUserCount is a free data retrieval call binding the contract method 0xb5cb15f7.
//
// Solidity: function getUserCount() constant returns(count uint256)
func (_EthVC *EthVCCallerSession) GetUserCount() (*big.Int, error) {
	return _EthVC.Contract.GetUserCount(&_EthVC.CallOpts)
}

// GetUserEmailAtIndex is a free data retrieval call binding the contract method 0x7583e1aa.
//
// Solidity: function getUserEmailAtIndex(index uint256) constant returns(email bytes32)
func (_EthVC *EthVCCaller) GetUserEmailAtIndex(opts *bind.CallOpts, index *big.Int) ([32]byte, error) {
	var (
		ret0 = new([32]byte)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "getUserEmailAtIndex", index)
	return *ret0, err
}

// GetUserEmailAtIndex is a free data retrieval call binding the contract method 0x7583e1aa.
//
// Solidity: function getUserEmailAtIndex(index uint256) constant returns(email bytes32)
func (_EthVC *EthVCSession) GetUserEmailAtIndex(index *big.Int) ([32]byte, error) {
	return _EthVC.Contract.GetUserEmailAtIndex(&_EthVC.CallOpts, index)
}

// GetUserEmailAtIndex is a free data retrieval call binding the contract method 0x7583e1aa.
//
// Solidity: function getUserEmailAtIndex(index uint256) constant returns(email bytes32)
func (_EthVC *EthVCCallerSession) GetUserEmailAtIndex(index *big.Int) ([32]byte, error) {
	return _EthVC.Contract.GetUserEmailAtIndex(&_EthVC.CallOpts, index)
}

// GetUserRightLevelAtIndex is a free data retrieval call binding the contract method 0x01456d21.
//
// Solidity: function getUserRightLevelAtIndex(index uint256) constant returns(rightLevel uint256)
func (_EthVC *EthVCCaller) GetUserRightLevelAtIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "getUserRightLevelAtIndex", index)
	return *ret0, err
}

// GetUserRightLevelAtIndex is a free data retrieval call binding the contract method 0x01456d21.
//
// Solidity: function getUserRightLevelAtIndex(index uint256) constant returns(rightLevel uint256)
func (_EthVC *EthVCSession) GetUserRightLevelAtIndex(index *big.Int) (*big.Int, error) {
	return _EthVC.Contract.GetUserRightLevelAtIndex(&_EthVC.CallOpts, index)
}

// GetUserRightLevelAtIndex is a free data retrieval call binding the contract method 0x01456d21.
//
// Solidity: function getUserRightLevelAtIndex(index uint256) constant returns(rightLevel uint256)
func (_EthVC *EthVCCallerSession) GetUserRightLevelAtIndex(index *big.Int) (*big.Int, error) {
	return _EthVC.Contract.GetUserRightLevelAtIndex(&_EthVC.CallOpts, index)
}

// IsUserKnown is a free data retrieval call binding the contract method 0x6224f36f.
//
// Solidity: function isUserKnown(userAddress address) constant returns(isKnown bool)
func (_EthVC *EthVCCaller) IsUserKnown(opts *bind.CallOpts, userAddress common.Address) (bool, error) {
	var (
		ret0 = new(bool)
	)
	out := ret0
	err := _EthVC.contract.Call(opts, out, "isUserKnown", userAddress)
	return *ret0, err
}

// IsUserKnown is a free data retrieval call binding the contract method 0x6224f36f.
//
// Solidity: function isUserKnown(userAddress address) constant returns(isKnown bool)
func (_EthVC *EthVCSession) IsUserKnown(userAddress common.Address) (bool, error) {
	return _EthVC.Contract.IsUserKnown(&_EthVC.CallOpts, userAddress)
}

// IsUserKnown is a free data retrieval call binding the contract method 0x6224f36f.
//
// Solidity: function isUserKnown(userAddress address) constant returns(isKnown bool)
func (_EthVC *EthVCCallerSession) IsUserKnown(userAddress common.Address) (bool, error) {
	return _EthVC.Contract.IsUserKnown(&_EthVC.CallOpts, userAddress)
}

// AddAuthorizedUser is a paid mutator transaction binding the contract method 0x556cb493.
//
// Solidity: function addAuthorizedUser(newUserAddress address, email bytes32, rightLevel uint256) returns()
func (_EthVC *EthVCTransactor) AddAuthorizedUser(opts *bind.TransactOpts, newUserAddress common.Address, email [32]byte, rightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.contract.Transact(opts, "addAuthorizedUser", newUserAddress, email, rightLevel)
}

// AddAuthorizedUser is a paid mutator transaction binding the contract method 0x556cb493.
//
// Solidity: function addAuthorizedUser(newUserAddress address, email bytes32, rightLevel uint256) returns()
func (_EthVC *EthVCSession) AddAuthorizedUser(newUserAddress common.Address, email [32]byte, rightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddAuthorizedUser(&_EthVC.TransactOpts, newUserAddress, email, rightLevel)
}

// AddAuthorizedUser is a paid mutator transaction binding the contract method 0x556cb493.
//
// Solidity: function addAuthorizedUser(newUserAddress address, email bytes32, rightLevel uint256) returns()
func (_EthVC *EthVCTransactorSession) AddAuthorizedUser(newUserAddress common.Address, email [32]byte, rightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddAuthorizedUser(&_EthVC.TransactOpts, newUserAddress, email, rightLevel)
}

// AddBranch is a paid mutator transaction binding the contract method 0xf8f8e53c.
//
// Solidity: function addBranch(rightLevel uint256, name bytes32, creationDateUnix uint256) returns()
func (_EthVC *EthVCTransactor) AddBranch(opts *bind.TransactOpts, rightLevel *big.Int, name [32]byte, creationDateUnix *big.Int) (*types.Transaction, error) {
	return _EthVC.contract.Transact(opts, "addBranch", rightLevel, name, creationDateUnix)
}

// AddBranch is a paid mutator transaction binding the contract method 0xf8f8e53c.
//
// Solidity: function addBranch(rightLevel uint256, name bytes32, creationDateUnix uint256) returns()
func (_EthVC *EthVCSession) AddBranch(rightLevel *big.Int, name [32]byte, creationDateUnix *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddBranch(&_EthVC.TransactOpts, rightLevel, name, creationDateUnix)
}

// AddBranch is a paid mutator transaction binding the contract method 0xf8f8e53c.
//
// Solidity: function addBranch(rightLevel uint256, name bytes32, creationDateUnix uint256) returns()
func (_EthVC *EthVCTransactorSession) AddBranch(rightLevel *big.Int, name [32]byte, creationDateUnix *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddBranch(&_EthVC.TransactOpts, rightLevel, name, creationDateUnix)
}

// AddCommit is a paid mutator transaction binding the contract method 0xc066ea0a.
//
// Solidity: function addCommit(branchName bytes32, indexFileUpper bytes32, indexFileLower bytes32, revision uint256) returns()
func (_EthVC *EthVCTransactor) AddCommit(opts *bind.TransactOpts, branchName [32]byte, indexFileUpper [32]byte, indexFileLower [32]byte, revision *big.Int) (*types.Transaction, error) {
	return _EthVC.contract.Transact(opts, "addCommit", branchName, indexFileUpper, indexFileLower, revision)
}

// AddCommit is a paid mutator transaction binding the contract method 0xc066ea0a.
//
// Solidity: function addCommit(branchName bytes32, indexFileUpper bytes32, indexFileLower bytes32, revision uint256) returns()
func (_EthVC *EthVCSession) AddCommit(branchName [32]byte, indexFileUpper [32]byte, indexFileLower [32]byte, revision *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddCommit(&_EthVC.TransactOpts, branchName, indexFileUpper, indexFileLower, revision)
}

// AddCommit is a paid mutator transaction binding the contract method 0xc066ea0a.
//
// Solidity: function addCommit(branchName bytes32, indexFileUpper bytes32, indexFileLower bytes32, revision uint256) returns()
func (_EthVC *EthVCTransactorSession) AddCommit(branchName [32]byte, indexFileUpper [32]byte, indexFileLower [32]byte, revision *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.AddCommit(&_EthVC.TransactOpts, branchName, indexFileUpper, indexFileLower, revision)
}

// DeleteUser is a paid mutator transaction binding the contract method 0x5c60f226.
//
// Solidity: function deleteUser(userToDelete address) returns()
func (_EthVC *EthVCTransactor) DeleteUser(opts *bind.TransactOpts, userToDelete common.Address) (*types.Transaction, error) {
	return _EthVC.contract.Transact(opts, "deleteUser", userToDelete)
}

// DeleteUser is a paid mutator transaction binding the contract method 0x5c60f226.
//
// Solidity: function deleteUser(userToDelete address) returns()
func (_EthVC *EthVCSession) DeleteUser(userToDelete common.Address) (*types.Transaction, error) {
	return _EthVC.Contract.DeleteUser(&_EthVC.TransactOpts, userToDelete)
}

// DeleteUser is a paid mutator transaction binding the contract method 0x5c60f226.
//
// Solidity: function deleteUser(userToDelete address) returns()
func (_EthVC *EthVCTransactorSession) DeleteUser(userToDelete common.Address) (*types.Transaction, error) {
	return _EthVC.Contract.DeleteUser(&_EthVC.TransactOpts, userToDelete)
}

// UpdateUser is a paid mutator transaction binding the contract method 0xcc32588a.
//
// Solidity: function updateUser(userToUpdate address, newEmail bytes32, newRightLevel uint256) returns()
func (_EthVC *EthVCTransactor) UpdateUser(opts *bind.TransactOpts, userToUpdate common.Address, newEmail [32]byte, newRightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.contract.Transact(opts, "updateUser", userToUpdate, newEmail, newRightLevel)
}

// UpdateUser is a paid mutator transaction binding the contract method 0xcc32588a.
//
// Solidity: function updateUser(userToUpdate address, newEmail bytes32, newRightLevel uint256) returns()
func (_EthVC *EthVCSession) UpdateUser(userToUpdate common.Address, newEmail [32]byte, newRightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.UpdateUser(&_EthVC.TransactOpts, userToUpdate, newEmail, newRightLevel)
}

// UpdateUser is a paid mutator transaction binding the contract method 0xcc32588a.
//
// Solidity: function updateUser(userToUpdate address, newEmail bytes32, newRightLevel uint256) returns()
func (_EthVC *EthVCTransactorSession) UpdateUser(userToUpdate common.Address, newEmail [32]byte, newRightLevel *big.Int) (*types.Transaction, error) {
	return _EthVC.Contract.UpdateUser(&_EthVC.TransactOpts, userToUpdate, newEmail, newRightLevel)
}

// EthVCUserEventIterator is returned from FilterUserEvent and is used to iterate over the raw logs and unpacked data for UserEvent events raised by the EthVC contract.
type EthVCUserEventIterator struct {
	Event *EthVCUserEvent // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *EthVCUserEventIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(EthVCUserEvent)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(EthVCUserEvent)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *EthVCUserEventIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *EthVCUserEventIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// EthVCUserEvent represents a UserEvent event raised by the EthVC contract.
type EthVCUserEvent struct {
	UserAdress  common.Address
	SuccessCode *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterUserEvent is a free log retrieval operation binding the contract event 0x1fb035d1f559adcc493bdcf3a474e65352c4bb4d8d6ab9ad62fab2386bb439ea.
//
// Solidity: e UserEvent(userAdress address, successCode uint256)
func (_EthVC *EthVCFilterer) FilterUserEvent(opts *bind.FilterOpts) (*EthVCUserEventIterator, error) {

	logs, sub, err := _EthVC.contract.FilterLogs(opts, "UserEvent")
	if err != nil {
		return nil, err
	}
	return &EthVCUserEventIterator{contract: _EthVC.contract, event: "UserEvent", logs: logs, sub: sub}, nil
}

// WatchUserEvent is a free log subscription operation binding the contract event 0x1fb035d1f559adcc493bdcf3a474e65352c4bb4d8d6ab9ad62fab2386bb439ea.
//
// Solidity: e UserEvent(userAdress address, successCode uint256)
func (_EthVC *EthVCFilterer) WatchUserEvent(opts *bind.WatchOpts, sink chan<- *EthVCUserEvent) (event.Subscription, error) {

	logs, sub, err := _EthVC.contract.WatchLogs(opts, "UserEvent")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(EthVCUserEvent)
				if err := _EthVC.contract.UnpackLog(event, "UserEvent", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// EthVCVCEventIterator is returned from FilterVCEvent and is used to iterate over the raw logs and unpacked data for VCEvent events raised by the EthVC contract.
type EthVCVCEventIterator struct {
	Event *EthVCVCEvent // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *EthVCVCEventIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(EthVCVCEvent)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(EthVCVCEvent)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *EthVCVCEventIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *EthVCVCEventIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// EthVCVCEvent represents a VCEvent event raised by the EthVC contract.
type EthVCVCEvent struct {
	BranchName  [32]byte
	SuccessCode *big.Int
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterVCEvent is a free log retrieval operation binding the contract event 0x854cd08e1ad382bca289108e982bf8d02eb4556c7e5c3f48ec720d545d6157ac.
//
// Solidity: e VCEvent(branchName bytes32, successCode uint256)
func (_EthVC *EthVCFilterer) FilterVCEvent(opts *bind.FilterOpts) (*EthVCVCEventIterator, error) {

	logs, sub, err := _EthVC.contract.FilterLogs(opts, "VCEvent")
	if err != nil {
		return nil, err
	}
	return &EthVCVCEventIterator{contract: _EthVC.contract, event: "VCEvent", logs: logs, sub: sub}, nil
}

// WatchVCEvent is a free log subscription operation binding the contract event 0x854cd08e1ad382bca289108e982bf8d02eb4556c7e5c3f48ec720d545d6157ac.
//
// Solidity: e VCEvent(branchName bytes32, successCode uint256)
func (_EthVC *EthVCFilterer) WatchVCEvent(opts *bind.WatchOpts, sink chan<- *EthVCVCEvent) (event.Subscription, error) {

	logs, sub, err := _EthVC.contract.WatchLogs(opts, "VCEvent")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(EthVCVCEvent)
				if err := _EthVC.contract.UnpackLog(event, "VCEvent", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}
