package test

import (
	"context"
	crypto2 "crypto"
	"crypto/ecdsa"
	"ethVC/ContractImpl"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
	"testing"
)

type TestEnv struct {
	publicKey        crypto2.PublicKey
	walletAddress    common.Address
	client           *ethclient.Client
	contractAddress  common.Address
	contractInstance *ContractImpl.EthVC
	auth             *bind.TransactOpts
}

func setUpLocalTestEnvironment() (env *TestEnv) {
	client, err := ethclient.Dial("http://127.0.0.1:8543")
	if err != nil {
		log.Fatal(err)
	}

	contractAddress := common.HexToAddress("0x76660998687792dbC8181F9a8865EF6De2aA50a5")
	instance, err := ContractImpl.NewEthVC(contractAddress, client)
	if err != nil {
		log.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA("e62e873eb242a90d48f127f7fcd74c75565d75573b27bf6bab06c5bc6f5dc673")
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(20000000)
	auth.GasPrice = gasPrice

	return &TestEnv{publicKey, fromAddress, client, contractAddress, instance, auth}
}

func TestLocalGetUserCount(t *testing.T) {
	testEnv := setUpLocalTestEnvironment()

	res, err := testEnv.contractInstance.GetUserCount(nil)
	if err != nil {
		t.Error(err)
	}
	if !(res.Uint64() == 1) {
		t.Fail()
	}
}

func TestLocalIsUserKnown(t *testing.T) {
	testEnv := setUpLocalTestEnvironment()

	res, err := testEnv.contractInstance.IsUserKnown(nil, testEnv.walletAddress)
	if err != nil {
		t.Error(err)
	}

	if res == false {
		t.Fail()
	}

	key, _ := crypto.GenerateKey()
	nonExistentAddress := crypto.PubkeyToAddress(key.PublicKey)

	res, err = testEnv.contractInstance.IsUserKnown(nil, nonExistentAddress)
	if err != nil {
		t.Error(err)
	}

	if res == true {
		t.Fail()
	}
}
