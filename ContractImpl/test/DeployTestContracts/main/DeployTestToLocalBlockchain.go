package main

import (
	"context"
	"crypto/ecdsa"
	"ethVC/ContractImpl"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
)

/*
Setup local chain:
https://hackernoon.com/set-up-a-private-ethereum-blockchain-and-deploy-your-first-solidity-smart-contract-on-the-caa8334c343d


 personal.newAccount('seed') // seed is password
Account address: "0x33a0ed7d4deacb2d49ecca8026616a8a13e84041"

private key: e62e873eb242a90d48f127f7fcd74c75565d75573b27bf6bab06c5bc6f5dc673
address: 0x33A0ed7D4DEAcB2d49eCca8026616a8A13e84041


V1:
contract address: 0x8B7528e1a4550f44193E8d8e62b1079770E369FF
transaction: 0x2fb0508c8113a63a00ca81e9733fb9845d4c4ba9efbfb1ae0d49f95a4c0adccf

V2:
contract address: 0x76660998687792dbC8181F9a8865EF6De2aA50a5
transaction: 0x87e36a87e696134472c4077f09df477c3263c9f01647628ac4b4868d9b450544

V3:
0xe07b54Af97BAF26edCBE5D46a8B5930a27DF85af
0x539684978435d26c0dea331c9859225f62cbd743ef2424d31483bdc65e9272e7

V4:
0x3fb328ecED8dB09E6C6061A3eFEBc30613e4cC39
0x57e2e81a3243b12ae06447e62c07b98f7f68a0b22fcd4cf0aeb1d305ee3761ad


*/

func main() {
	client, err := ethclient.Dial("http://127.0.0.1:8543")

	if err != nil {
		log.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA("e62e873eb242a90d48f127f7fcd74c75565d75573b27bf6bab06c5bc6f5dc673")
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(20000000)
	auth.GasPrice = gasPrice

	email := [32]byte{}
	copy(email[:], []byte("foo@bar.com"))

	address, tx, _, err := ContractImpl.DeployEthVC(auth, client, email)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(address.Hex())
	fmt.Println(tx.Hash().Hex())
}
