package main

import (
	"context"
	"crypto/ecdsa"
	"ethVC/ContractImpl"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
)

var AddressHex = "fad9c8855b740a0b7ed4c221dbad0f33a83a49cad6b3fe8d5817ac83d38b6a19"

func main() {
	client, err := ethclient.Dial("https://rinkeby.infura.io")

	if err != nil {
		log.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA(AddressHex)
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(6300000)
	auth.GasPrice = gasPrice

	email := [32]byte{}
	copy(email[:], []byte("foo@bar.com"))

	address, tx, _, err := ContractImpl.DeployEthVC(auth, client, email)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(address.Hex())
	fmt.Println(tx.Hash().Hex())
}

/*
0x440ff76E2B2A93713BE0Fb9ec81A160911cC9995
0xce7a5e165acc39653bbd1be233ffef6f4d37e31c0cced30c57f5609604d6f9cb

0x440ff76E2B2A93713BE0Fb9ec81A160911cC9995
0xbe2206ee77595aeb8494dd10695602269320ab90159680d813e992b6d6efbbce


*/
