package test

import (
	"context"
	"ethVC/ContractImpl"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/accounts/abi/bind/backends"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/crypto"
	"log"
	"math/big"
)

type SimulatedTestEnv struct {
	contract        *ContractImpl.EthVC
	auth            *bind.TransactOpts
	deployerAddress common.Address
	contractAddress common.Address
	client          *backends.SimulatedBackend
}

func setUpSimulatedTestEnvironment() (testEnv *SimulatedTestEnv) {
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)

	balance := new(big.Int)
	balance.SetString("10000000000000000000", 10) // 10 eth in wei

	address := auth.From
	genesisAlloc := map[common.Address]core.GenesisAccount{
		address: {
			Balance: balance,
		},
	}

	blockGasLimit := uint64(4712388)
	client := backends.NewSimulatedBackend(genesisAlloc, blockGasLimit)

	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(3000000)
	auth.GasPrice, err = client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)

	}

	email := [32]byte{}
	copy(email[:], []byte("foo@bar.com"))

	contractAddress, _, contract, e := ContractImpl.DeployEthVC(auth, client, email)
	if e != nil {
		log.Fatal(e)

	}
	client.Commit()
	//return c, auth, e, address, contractAddress, client
	return &SimulatedTestEnv{contract, auth, address, contractAddress, client}
}
