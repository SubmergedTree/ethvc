package main

import (
	"context"
	"ethVC/ContractImpl"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
	"strings"
)

func main() {
	client, err := ethclient.Dial("http://127.0.0.1:8543")
	if err != nil {
		log.Fatal(err)
	}

	contractAddress := common.HexToAddress("0x76660998687792dbC8181F9a8865EF6De2aA50a5")
	query := ethereum.FilterQuery{
		Addresses: []common.Address{
			contractAddress,
		},
	}

	logs, err := client.FilterLogs(context.Background(), query)
	if err != nil {
		log.Fatal(err)
	}

	contractAbi, err := abi.JSON(strings.NewReader(string(ContractImpl.EthVCABI)))
	if err != nil {
		log.Fatal(err)
	}

	for _, vLog := range logs {
		/*fmt.Println(vLog.BlockHash.Hex())
		fmt.Println(vLog.BlockNumber)
		fmt.Println(vLog.TxHash.Hex())*/

		event := struct {
			UserAdress  common.Address
			SuccessCode *big.Int
		}{}
		err := contractAbi.Unpack(&event, "UserEvent", vLog.Data)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(event.UserAdress.String()))
		fmt.Println(string(event.SuccessCode.String()))

		var topics [4]string
		for i := range vLog.Topics {
			topics[i] = vLog.Topics[i].Hex()
		}

		//fmt.Println(topics[0])
	}

	/*eventSignature := []byte("UserEvent(address,uint)")
	hash := crypto.Keccak256Hash(eventSignature)
	fmt.Println(hash.Hex())*/
}
