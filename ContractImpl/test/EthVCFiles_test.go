package test

import (
	"github.com/ethereum/go-ethereum/common"
	"math/big"
	"testing"
)

func TestGetInitialBranchCount(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	res, err := env.contract.GetBranchCount(nil)
	if err != nil {
		t.Error(err)
	}
	if res.Uint64() != 0 {
		t.Fail()
	}
}

func TestCreateBranch(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	branchName := [32]byte{}
	copy(branchName[:], []byte("test"))

	env.contract.CreateBranch(env.auth, big.NewInt(1), branchName, big.NewInt(1542557042))

	env.client.Commit()

	res, err := env.contract.GetBranchCount(nil)
	if err != nil {
		t.Error(err)
	}
	if res.Uint64() != 1 {
		t.Fail()
	}
}

func TestAddIndexFile(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	branchName := [32]byte{}
	copy(branchName[:], []byte("test"))

	rightLevel := big.NewInt(0)
	unixTime := big.NewInt(1542557042)

	env.contract.CreateBranch(env.auth, rightLevel, branchName, unixTime)

	env.client.Commit()

	fileAddress := common.HexToAddress("0x76660998687792dbC8181F9a8865EF6De2aA50a5")

	env.contract.AddIndexFile(env.auth, big.NewInt(0), fileAddress)

	env.client.Commit()

	receivedBranchName, receivedTimeStamp, receivedAccessRight, receivedAddresses, err := env.contract.GetBranchAtIndex(nil, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	if receivedBranchName != branchName || receivedTimeStamp.Uint64() != unixTime.Uint64() ||
		receivedAccessRight.Uint64() != rightLevel.Uint64() {
		t.Fail()
	}

	if receivedAddresses[0].String() != fileAddress.String() {
		t.Fail()
	}

}
