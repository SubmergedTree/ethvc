package test

/*
import (
	"context"
	"crypto/ecdsa"
	"ethVC/ContractImpl"
	"fmt"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"log"
	"math/big"
	"strings"
	"testing"
	"time"
)

func setUp() *ethclient.Client{
	client, err := ethclient.Dial("wss://rinkeby.infura.io/ws")
	if err != nil {
		log.Fatal(err)
	}
	return client
}

func TestAddUserRinkeby(t *testing.T) {
	client := setUp()


	contractAddress := common.HexToAddress("0x440ff76E2B2A93713BE0Fb9ec81A160911cC9995")

	privateKey, err := crypto.HexToECDSA("fad9c8855b740a0b7ed4c221dbad0f33a83a49cad6b3fe8d5817ac83d38b6a19")
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}


	auth := bind.NewKeyedTransactor(privateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)     // in wei
	auth.GasLimit = uint64(300000) // in units
	auth.GasPrice = gasPrice


	go func() {
		query := ethereum.FilterQuery{
			Addresses: []common.Address{contractAddress},
		}

		logs := make(chan types.Log)
		sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
		if err != nil {
			log.Fatal(err)
		}

		contractAbi, err := abi.JSON(strings.NewReader(string(ContractImpl.EthVCABI)))
		if err != nil {
			log.Fatal(err)
		}


		for {
			select {
			case err := <-sub.Err():
				log.Fatal(err)
			case vLog := <-logs:
				event := struct {
					userAddress common.Address
					successCode uint
				}{}
				err := contractAbi.Unpack(&event, "UserEvent", vLog.Data)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Println(event.userAddress.String())
				fmt.Println(event.successCode)
			}
		}
	}()

	key, _ := crypto.GenerateKey()
	address := crypto.PubkeyToAddress(key.PublicKey)

	email := [32]byte{}
	copy(email[:], []byte("hello@world.com"))
	instance, err := ContractImpl.NewEthVC(address, client)

	_, err = instance.AddAuthorizedUser(auth, address, email, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	time.Sleep(10000000000)
}

*/
