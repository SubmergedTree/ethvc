package test

import (
	"github.com/ethereum/go-ethereum/crypto"
	"math/big"
	"testing"
)

func TestGetUserCount(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	res, err := env.contract.GetUserCount(nil)
	if err != nil {
		t.Error(err)
	}
	if !(res.Uint64() == 1) {
		t.Fail()
	}
}

func TestIsUserKnown(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	res, err := env.contract.IsUserKnown(nil, env.deployerAddress)
	if err != nil {
		t.Error(err)
	}

	if res == false {
		t.Fail()
	}

	key, _ := crypto.GenerateKey()
	nonExistentAddress := crypto.PubkeyToAddress(key.PublicKey)

	res, err = env.contract.IsUserKnown(nil, nonExistentAddress)
	if err != nil {
		t.Error(err)
	}

	if res == true {
		t.Fail()
	}
}

func TestGetUserAtIndex(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	returnedAddress, err := env.contract.GetUserAtIndex(nil, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	if env.deployerAddress.String() != returnedAddress.String() {
		t.Fail()
	}
}

func TestGetUserEmailAtIndex(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	returnedEmail, err := env.contract.GetUserEmailAtIndex(nil, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	email := [32]byte{}
	copy(email[:], []byte("foo@bar.com"))

	if returnedEmail != email {
		t.Fail()
	}
}

func TestGetUserRightLevelAtIndex(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	returnedRightLevel, err := env.contract.GetUserRightLevelAtIndex(nil, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	if returnedRightLevel.Uint64() != 1 {
		t.Fail()
	}
}

func TestAddUser(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	key, _ := crypto.GenerateKey()
	address := crypto.PubkeyToAddress(key.PublicKey)

	email := [32]byte{}
	copy(email[:], []byte("fizz@bazz.com"))

	_, err := env.contract.AddAuthorizedUser(env.auth, address, email, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	env.client.Commit()

	res, err := env.contract.GetUserCount(nil)
	if err != nil {
		t.Error(err)
	}

	if !(res.Uint64() == 2) {
		t.Error(res.Uint64())
	}

	// Will maybe be removed
	isKnown, err := env.contract.IsUserKnown(nil, address)
	if !isKnown {
		t.Error(err)
	}

	returnedAddress, err := env.contract.GetUserAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if address.String() != returnedAddress.String() {
		t.Error(returnedAddress.String())
	}

	returnedEmail, err := env.contract.GetUserEmailAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if returnedEmail != email {
		t.Error(string(returnedEmail[:]))
	}

	returnedRightLevel, err := env.contract.GetUserRightLevelAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if returnedRightLevel.Uint64() != 0 {
		t.Error(returnedRightLevel)
	}
}

func TestDeleteUser(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	key, _ := crypto.GenerateKey()
	address := crypto.PubkeyToAddress(key.PublicKey)

	email := [32]byte{}
	copy(email[:], []byte("fizz@bazz.com"))

	_, err := env.contract.AddAuthorizedUser(env.auth, address, email, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	env.client.Commit()

	res, err := env.contract.GetUserCount(nil)
	if err != nil {
		t.Error(err)
	}

	if !(res.Uint64() == 2) {
		t.Error(res.Uint64())
	}

	_, err = env.contract.DeleteUser(env.auth, address)
	if err != nil {
		t.Error(err)
	}

	env.client.Commit()

	res, err = env.contract.GetUserCount(nil)
	if err != nil {
		t.Error(err)
	}

	if !(res.Uint64() == 1) {
		t.Error(res.Uint64())
	}
}

func TestUpdateUser(t *testing.T) {
	env := setUpSimulatedTestEnvironment()

	key, _ := crypto.GenerateKey()
	address := crypto.PubkeyToAddress(key.PublicKey)

	email := [32]byte{}
	copy(email[:], []byte("fizz@bazz.com"))

	_, err := env.contract.AddAuthorizedUser(env.auth, address, email, big.NewInt(0))
	if err != nil {
		t.Error(err)
	}

	env.client.Commit()

	newEmail := [32]byte{}
	copy(newEmail[:], []byte("blub@blab.com"))

	_, err = env.contract.UpdateUser(env.auth, address, newEmail, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	env.client.Commit()

	returnedAddress, err := env.contract.GetUserAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if address.String() != returnedAddress.String() {
		t.Error(returnedAddress.String())
	}

	returnedEmail, err := env.contract.GetUserEmailAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if returnedEmail != newEmail {
		t.Error(string(returnedEmail[:]))
	}

	returnedRightLevel, err := env.contract.GetUserRightLevelAtIndex(nil, big.NewInt(1))
	if err != nil {
		t.Error(err)
	}

	if returnedRightLevel.Uint64() != 1 {
		t.Error(returnedRightLevel)
	}
}
